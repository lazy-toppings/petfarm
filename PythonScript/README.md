# config_parser

### Installation

```bash
pip install openpyxl
```

### Run

```bash
python config_parser.py 'path_to_xlsx' --pretty
# output will be config.json
```

- options:
  - `--pretty`: 사람이 읽기 쉽도록 파일 출력

### Config file

[config.xlsx](https://drive.google.com/drive/folders/1_clcuINuOjBgFoPuiQs4FXY5zfypTNh1)

