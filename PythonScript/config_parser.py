import openpyxl
import sys
import re


class Building:
    def __init__(self, cells):
        self.name = cells[0].value
        self.index = cells[1].value
        self.price = cells[2].value

class Item:
    def __init__(self, cells):
        self.name = cells[0].value
        self.index = f'"{cells[1].value}"'
        self.price = cells[2].value
        self.hungry = cells[3].value
        self.clean = cells[4].value
        self.fun = cells[5].value

class Pet:
    def __init__(self, cells):
        self.name = cells[0].value
        self.index = cells[1].value
        self.favorite_items = [str(item_dict[item.strip()]) for item in cells[2].value.split(',')]
        self.dislike_items = [str(item_dict[item.strip()]) for item in cells[3].value.split(',')]
        self.hungry = {
            'min': cells[4].value,
            'max': cells[5].value
        }
        self.clean = {
            'min': cells[6].value,
            'max': cells[7].value
        }
        self.fun = {
            'min': cells[8].value,
            'max': cells[9].value
        }
        self.fences = [str(building_dict[item.strip()]) for item in cells[10].value.split(',')]
        self.exp = cells[11].value
        self.reward = cells[12].value
        self.level = cells[13].value
        self.chain = cells[14].value

class Level:
    def __init__(self, cells):
        self.exp = [int(cell.value) for cell in cells[1:]]

class Facility:    
    def accept_price(self, cells):
        self.price = [str(cell.value) for cell in cells[1:]]
    def accept_time(self, cells):
        self.time = [str(cell.value) for cell in cells[1:]]

class House(Facility):
    def accept_hungry(self, cells):
        self.hungry = [str(cell.value) for cell in cells[1:]]
    def accept_fun(self, cells):
        self.fun = [str(cell.value) for cell in cells[1:]]
    def accept_clean(self, cells):
        self.clean = [str(cell.value) for cell in cells[1:]]
    def accept_spot(self, cells):
        self.spot = [str(cell.value) for cell in cells[1:]]

class Bank(Facility):
    def accept_money(self, cells):
        self.money = [
            {
                'min': cell.value.split('~')[0],
                'max': cell.value.split('~')[-1]
            } 
            for cell in cells[1:]
        ]

class Factory(Facility):
    def accept_discount(self, cells):
        self.discount = [str(cell.value) for cell in cells[1:]]


pretty = False
xlsx = './config.xlsx'
if len(sys.argv) > 1:
    for cmd in sys.argv[1:]:
        if cmd.endswith('.xlsx'):
            xlsx = cmd
        if cmd == '--pretty':
            pretty = True
print(xlsx)
wb = openpyxl.load_workbook(xlsx)
pet_sheet = wb['Pet']
item_sheet = wb['Item']
building_sheet = wb['Building']
level_sheet = wb['Level']

building_dict = dict()
building_list = list()
item_dict = dict()
item_list = list()
pet_list = list()

it = level_sheet.rows
next(it)
level = Level(next(it))
house = House()
house.accept_hungry(next(it))
house.accept_fun(next(it))
house.accept_clean(next(it))
house.accept_price(next(it))
house.accept_spot(next(it))
bank = Bank()
bank.accept_money(next(it))
bank.accept_time(next(it))
bank.accept_price(next(it))
factory = Factory()
factory.accept_discount(next(it))
factory.accept_time(next(it))
factory.accept_price(next(it))

for i, row in enumerate(building_sheet):
    if i == 0:
        continue
    building = Building(row)
    building_dict[building.name] = building.index
    building_list.append(building)
    

for i, row in enumerate(item_sheet):
    if i == 0:
        continue
    item = Item(row)
    item_dict[item.name] = item.index
    item_list.append(item)

try:
    for i, row in enumerate(pet_sheet):
        if i == 0:
            continue
        pet = Pet(row)
        pet_list.append(pet)

    json = '{\n'
    json += f'\t"level": {{\n'
    json += f'\t\t"exp": [\n'
    json += f'\t\t\t{", ".join(level.exp)}\n'
    json += f'\t\t]\n'
    json += f'\t}},\n'
    json += f'\t"house": {{\n'
    json += f'\t\t"hungry": [\n'
    json += f'\t\t\t{", ".join(house.hungry)}\n'
    json += f'\t\t],\n'
    json += f'\t\t"fun": [\n'
    json += f'\t\t\t{", ".join(house.fun)}\n'
    json += f'\t\t],\n'
    json += f'\t\t"clean": [\n'
    json += f'\t\t\t{", ".join(house.clean)}\n'
    json += f'\t\t],\n'
    json += f'\t\t"price": [\n'
    json += f'\t\t\t{", ".join(house.price)}\n'
    json += f'\t\t],\n'
    json += f'\t\t"spot": [\n'
    json += f'\t\t\t{", ".join(house.spot)}\n'
    json += f'\t\t]\n'
    json += f'\t}},\n'
    json += f'\t"bank": {{\n'
    json += f'\t\t"money": [\n'
    for i, bounary in enumerate(bank.money):
        json += f'\t\t\t{{\n'
        json += f'\t\t\t\t"min": {bounary["min"]},\n'
        json += f'\t\t\t\t"max": {bounary["max"]}\n'
        json += f'\t\t\t}}'
        if i < len(bank.money) - 1:
            json += ','
        json += '\n'
    json += f'\t\t],\n'
    json += f'\t\t"price": [\n'
    json += f'\t\t\t{", ".join(bank.price)}\n'
    json += f'\t\t],\n'
    json += f'\t\t"time": [\n'
    json += f'\t\t\t{", ".join(bank.time)}\n'
    json += f'\t\t]\n'
    json += f'\t}},\n'
    json += f'\t"factory": {{\n'
    json += f'\t\t"discount": [\n'
    json += f'\t\t\t{", ".join(factory.discount)}\n'
    json += f'\t\t],\n'
    json += f'\t\t"price": [\n'
    json += f'\t\t\t{", ".join(factory.price)}\n'
    json += f'\t\t],\n'
    json += f'\t\t"time": [\n'
    json += f'\t\t\t{", ".join(factory.time)}\n'
    json += f'\t\t]\n'
    json += f'\t}},\n'
    json += '\t"fenceList": [\n'
    for i, building in enumerate(building_list):
        if i < 4:
            continue
        json += '\t\t{\n'
        json += f'\t\t\t"_comment": "{building.name}",\n' if pretty else ''
        json += f'\t\t\t"type": {building.index},\n'
        json += f'\t\t\t"price": {building.price}\n'
        json += '\t\t}'
        if i + 1 < len(building_list):
            json += ','
        json += '\n'
    json += '\t],\n'    
    json += '\t"itemList": [\n'
    for i, item in enumerate(item_list):
        json += '\t\t{\n'
        json += f'\t\t\t"_comment": "{item.name}",\n' if pretty else ''
        json += f'\t\t\t"type": {item.index},\n'
        json += f'\t\t\t"price": {item.price},\n'
        json += f'\t\t\t"status": {{\n'
        json += f'\t\t\t\t"hungry": {item.hungry},\n'
        json += f'\t\t\t\t"clean": {item.clean},\n'
        json += f'\t\t\t\t"fun": {item.fun}\n'
        json += f'\t\t\t}}\n'
        json += '\t\t}'
        if i + 1 < len(item_list):
            json += ','
        json += '\n'
    json += '\t],\n'
    json += '\t"petList": [\n'
    for i, pet in enumerate(pet_list):
        json += '\t\t{\n'
        json += f'\t\t\t"_comment": "{pet.name}",\n' if pretty else ''
        json += f'\t\t\t"type": "{pet.index}",\n'
        json += f'\t\t\t"favoriteItems": [\n'
        json += f'\t\t\t\t{", ".join(pet.favorite_items)}\n'
        json += f'\t\t\t],\n'
        json += f'\t\t\t"dislikeItems": [\n'
        json += f'\t\t\t\t{", ".join(pet.dislike_items)}\n'
        json += f'\t\t\t],\n'
        json += f'\t\t\t"reductionRange": {{\n'
        json += f'\t\t\t\t"hungry": {{\n'
        json += f'\t\t\t\t\t"min": {pet.hungry["min"]},\n'
        json += f'\t\t\t\t\t"max": {pet.hungry["max"]}\n'
        json += f'\t\t\t\t}},\n'
        json += f'\t\t\t\t"clean": {{\n'
        json += f'\t\t\t\t\t"min": {pet.clean["min"]},\n'
        json += f'\t\t\t\t\t"max": {pet.clean["max"]}\n'
        json += f'\t\t\t\t}},\n'
        json += f'\t\t\t\t"fun": {{\n'
        json += f'\t\t\t\t\t"min": {pet.fun["min"]},\n'
        json += f'\t\t\t\t\t"max": {pet.fun["max"]}\n'
        json += f'\t\t\t\t}}\n'
        json += f'\t\t\t}},\n'
        json += f'\t\t\t"fences": [\n'
        json += f'\t\t\t\t{", ".join(pet.fences)}\n'
        json += f'\t\t\t],\n'
        json += f'\t\t\t"exp": {pet.exp},\n'
        json += f'\t\t\t"reward": {pet.reward},\n'
        json += f'\t\t\t"level": {pet.level},\n'
        json += f'\t\t\t"chain": {str(pet.chain).lower()}\n'
        json += '\t\t}'
        if i + 1 < len(pet_list):
            json += ','
        json += '\n'
    json += '\t]\n'
    json += '}'
    if not pretty:
        json = re.sub('\s', '', json)
    with open('config.json', 'w+') as f:
        f.write(json)
except KeyError as e:
    print(e)

