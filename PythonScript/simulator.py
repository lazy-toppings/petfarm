import json
import random

global_id = 0
class Pet:
    def __init__(self, pet_type, duration):
        global global_id
        global_id += 1
        self.id = global_id
        self.config = pet_dict[pet_type]
        self.duration = duration
        self.hungry = 80
        self.clean = 80
        self.fun = 80
        self.cared = 0
        self.time = 0
    
    def status_down(self):
        st = random.randrange(0,3)
        if st == 0:
            rh = self.config['reductionRange']['hungry']
            self.hungry -= int(random.randrange(rh['min'], rh['max'] + 1) * 0.45)
            self.hungry = max(self.hungry, 0)
        elif st == 1:
            rc = self.config['reductionRange']['clean']
            self.clean -= int(random.randrange(rc['min'], rc['max'] + 1) * 0.45)
            self.clean = max(self.clean, 0)
        else:
            rf = self.config['reductionRange']['fun']        
            self.fun -= int(random.randrange(rf['min'], rf['max'] + 1) * 0.45)
            self.fun = max(self.fun, 0)
        self.time += 1
        if self.status() == 2:
            self.cared -= 1
            if min(self.hungry, self.clean, self.fun) == 0:
                self.cared -= 2

    def status(self):
        st = min(self.hungry, self.clean, self.fun)
        if st < 1:
            return 3
        if st < 30:
            return 2
        if st < 60:
            return 1
        return 0

    def consume(self, item_type):
        item_config = item_dict[item_type]
        factor = self.item_factor(item_type)
        h = int(item_config['status']['hungry'] * factor)
        c = int(item_config['status']['clean'] * factor)
        f = int(item_config['status']['fun'] * factor)
        self.hungry += h * 3
        self.clean += c * 3
        self.fun += f * 3

    def item_factor(self, item_type):
        if item_type in self.config["favoriteItems"]:
            return 1.2
        elif item_type in self.config["dislikeItems"]:
            return 0.3
        return 1
    
    def lack(self):        
        st = self.status()
        if st == 0:
            return -1
        st = 60 if st == 1 else 30
        if self.hungry < st:
            return 0
        if self.fun < st:
            return 1
        return 2
    
    def score(self):
        return int(((self.cared / self.time) + 3) / 3 * 100)
    
    def status_str(self):
        return f'{self.id:3d}[{self.config["_comment"]:^6}] {self.score():3d} ==> h: {self.hungry:3d}, f: {self.fun:3d}, c: {self.clean:3d}'



filename = 'config.json'
with open(filename) as f:
    config = json.load(f)

pet_dict = dict()
for pet_config in config['petList']:
    pet_dict[pet_config['type']] = pet_config

item_dict = dict()
for item_config in config['itemList']:
    item_dict[item_config['type']] = item_config

level = 1
spot = 12
play_time = 10 * 48 # sec

def generate_pets(size):
    pet_types = [*pet_dict]
    return [Pet(random.choice(pet_types), 10) for i in range(size)]

def get_item(seed):
    if seed == 0:
        return get_hugry_item()
    if seed == 1:
        return get_fun_item()
    return get_clean_item()

def get_hugry_item():
    return random.randrange(0, 2)

def get_fun_item():
    return random.randrange(8, 10)
    
def get_clean_item():
    return random.randrange(17, 18)

with open('log.txt', 'w') as f:
    pet_list = generate_pets(spot)
    prev = 0
    time_log = []
    total_amount = 0
    item_count = 0
    for t in range(play_time):
        for pet in pet_list:
            pet.status_down()
            # f.write(f'{pet.status_str()}\n')
        care_need = [pet for pet in pet_list if pet.status() > 0]
        if len(care_need) == 0:
            continue
        time_log.append(t-prev)
        prev = t
        random.shuffle(care_need)
        care_need.sort(key=lambda x: x.status())
        for _ in range(1):
            if len(care_need) == 0:
                break
            pet = care_need.pop()
            lack = pet.lack()
            item = get_item(lack)
            for _ in range(2):
                if pet.lack() != lack:
                    break
                pet.consume(item)
                total_amount += item_dict[item]['price']
                item_count += 1
                f.write(f'feed {item} at time {t}\n')
                f.write(f'{pet.status_str()}\n')
            if pet.status() == 2:
                care_need.append(pet)
            elif pet.status() == 1:
                try:
                    if pet.status() != care_need[-1].status():
                        care_need.insert(0, pet)
                    else:
                        care_need.append(pet)
                except:
                    care_need.append(pet)
    f.write(f'avg : {sum(time_log) / len(time_log)}\n')
    f.write(f'total price : {total_amount}\n')
    print(f'avg : {sum(time_log) / len(time_log)}')
    print(f'total price : {total_amount}')
    print(f'item count : {item_count}')
    

        
