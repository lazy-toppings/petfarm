﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class PlayData
{
    public int level;
    public int[] time; // { day, time }
    public int exp;
    public bool tutoDone;
    [NonSerialized]
    public LevelConfig config;
    [NonSerialized]
    private ShowLevelMng levelMng;

    public PlayData()
    {
        level = 0;
        time = new int[2] { 0, 0 };
    }

    public void AddExp(int amount)
    {
        exp += amount;
        if (exp > GetConfig().exp[GetConfig().exp.Count - 1]) exp = GetConfig().exp[GetConfig().exp.Count - 1];
        LevelCheck();
        GetLevelMng().UpdateExp();
    }

    public void DoneTuto()
    {
        tutoDone = true;
        Data.Instance.Save();
    }

    private void LevelCheck()
    {
        while (true)
        {
            if (level + 1 < GetConfig().exp.Count && exp >= GetConfig().exp[level + 1])
            {
                level++;
                GetLevelMng().ShowLevelUp();
                continue;
            }
            break;
        }
    }

    private ShowLevelMng GetLevelMng()
    {
        if (!levelMng)
        {
            levelMng = GameObject.Find("Level").gameObject.GetComponent<ShowLevelMng>();
        }

        return levelMng;
    }

    private LevelConfig GetConfig()
    {
        return config ?? (config = FarmConfig.Instance.LevelConfig);
    }

}