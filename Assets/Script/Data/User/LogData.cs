﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class LogData
{
    public int[] acceptPet;
    public int[] declinePet;
    public int[] makeGround;
    public int[] destroyGround;
    public int[] zeroStar;
    public int[] oneStar;
    public int[] twoStar;
    public int[] threeStar;
    public int[] perfectStar;
    public int[] buyItem;
    public int[] giveItem;
    public int[] missedItem;
    public int[] missedMoney;
    public int[] freeMoney;
    public Dictionary<string, int[]> acceptThePet;
    public Dictionary<string, int[]> buyTheItem;
    public Dictionary<string, int[]> giveTheItem;
    public int[] playTime;
    public int[] seqPlayTime;

    [NonSerialized]
    private MissionAlarmMng missionAlarmMng;
    [NonSerialized]
    private ShowMissionMng showMissionMng;

    public LogData()
    {
        // [0]: stack [1]: reset
        acceptPet = new int[2] { 0, 0 };
        declinePet = new int[2] { 0, 0 };
        makeGround = new int[2] { 0, 0 };
        destroyGround = new int[2] { 0, 0 };
        zeroStar = new int[2] { 0, 0 };
        oneStar = new int[2] { 0, 0 };
        twoStar = new int[2] { 0, 0 };
        threeStar = new int[2] { 0, 0 };
        perfectStar = new int[2] { 0, 0 };
        buyItem = new int[2] { 0, 0 };
        giveItem = new int[2] { 0, 0 };
        missedItem = new int[2] { 0, 0 };
        missedMoney = new int[2] { 0, 0 };
        freeMoney = new int[2] { 0, 0 };
        acceptThePet = new Dictionary<string, int[]>();
        buyTheItem = new Dictionary<string, int[]>();
        giveTheItem = new Dictionary<string, int[]>();
        playTime = new int[2] { 0, 0 };
        seqPlayTime = new int[2] { 0, 0 };
    }

    /* type의 펫을 수락했을 때 PetBuildMng.BuyPet()에서 */
    public void AcceptThePet(string type)
    {
        AcceptPet();
        if (acceptThePet.ContainsKey(type))
        {
            acceptThePet[type][0]++;
            acceptThePet[type][1]++;
        }
        else acceptThePet.Add(type, new int[2] { 1, 1 });

        //Debug.Log($"accept pet {type} for {acceptThePet[type]} times");
        IsMissionCleared(AMission.Type.ACCEPT_THE_PET, type, acceptThePet[type]);
    }

    /* 펫을 수락할 때 */
    private void AcceptPet()
    {
        acceptPet[0]++;
        acceptPet[1]++;

        //Debug.Log($"accpet pet for {acceptPet} times");
        IsMissionCleared(AMission.Type.ACCEPT_PET, acceptPet);
    }

    /* 펫을 거절할 때 MailMng.Declinepet()에서 */
    public void DeclinePet()
    {
        declinePet[0]++;
        declinePet[1]++;

        //Debug.Log($"decline pet for {declinePet} times");
        IsMissionCleared(AMission.Type.DECLINE_PET, declinePet);
    }

    /* 건물 지을 때 FenceBuildMng.BuyBuilding()에서 */
    public void MakeGround()
    {
        makeGround[0]++;
        makeGround[1]++;

        //Debug.Log($"make ground for {makeGround} times");
        IsMissionCleared(AMission.Type.MAKE_GROUND, makeGround);
    }

    /* 건물 파괴할 때 FenceBuildMng.BuyDestroy()에서 */
    public void DestroyGround()
    {
        destroyGround[0]++;
        destroyGround[1]++;

        //Debug.Log($"destroy ground for {destroyGround} times");
        IsMissionCleared(AMission.Type.DESTROY_GROUND, destroyGround);
    }

    /* 펫을 보내고 스타받을 때 RewardMng.SetInfos()에서 */
    public void SetStar(RankEnum r)
    {
        switch (r)
        {
            case RankEnum.S: PerfectStar(); ThreeStar(); break;
            case RankEnum.A: ThreeStar(); break;
            case RankEnum.B: TwoStar(); break;
            case RankEnum.C: OneStar(); break;
            case RankEnum.F: ZeroStar(); break;
        }
    }

    private void ZeroStar()
    {
        zeroStar[0]++;
        zeroStar[1]++;

        //Debug.Log($"zero star for {zeroStar} times");
        IsMissionCleared(AMission.Type.ZERO_STAR, zeroStar);
    }

    private void OneStar()
    {
        oneStar[0]++;
        oneStar[1]++;

        //Debug.Log($"one star for {oneStar} times");
        IsMissionCleared(AMission.Type.ONE_STAR, oneStar);
    }

    private void TwoStar()
    {
        twoStar[0]++;
        twoStar[1]++;

        //Debug.Log($"two star for {twoStar} times");
        IsMissionCleared(AMission.Type.TWO_STAR, twoStar);
    }

    private void ThreeStar()
    {
        threeStar[0]++;
        threeStar[1]++;

        //Debug.Log($"three star for {threeStar} times");
        IsMissionCleared(AMission.Type.THREE_STAR, threeStar);
    }

    private void PerfectStar()
    {
        perfectStar[0]++;
        perfectStar[1]++;

        //Debug.Log($"perfect star for {perfectStar} times");
        IsMissionCleared(AMission.Type.PERFECT_STAR, perfectStar);
    }

    /* type item을 구매할 때 ShopMng.BuyItem()에서 */
    public void BuyTheItem(string type)
    {
        BuyItem();
        if (buyTheItem.ContainsKey(type))
        {
            buyTheItem[type][0]++;
            buyTheItem[type][1]++;
        }
        else buyTheItem.Add(type, new int[2] { 1, 1 });
        IsMissionCleared(AMission.Type.BUY_THE_ITEM, type, buyTheItem[type]);
    }

    private void BuyItem()
    {
        buyItem[0]++;
        buyItem[1]++;

        //Debug.Log($"buy item for {buyItem} times");
        IsMissionCleared(AMission.Type.BUY_ITEM, buyItem);
    }

    /* type 아이템을 펫에게 주지 못했을 때 PetGiveMng.GetItem()에서 */
    public void MissItem()
    {
        missedItem[0]++;
        missedItem[1]++;

        //Debug.Log($"miss item for {missedItem} times");
        IsMissionCleared(AMission.Type.MISS_ITEM, missedItem);
    }

    /* type 아이템을 펫에게 줄 때 PetGiveMng.GetItem()에서 */
    public void GiveTheItem(string type)
    {
        GiveItem();

        if (giveTheItem.ContainsKey(type))
        {
            giveTheItem[type][0]++;
            giveTheItem[type][1]++;
        }
        else giveTheItem.Add(type, new int[2] { 1, 1 });
        IsMissionCleared(AMission.Type.GIVE_THE_ITEM, type, giveTheItem[type]);
    }

    private void GiveItem()
    {
        giveItem[0]++;
        giveItem[1]++;

        //Debug.Log($"give item for {giveItem} times");
        IsMissionCleared(AMission.Type.GIVE_ITEM, giveItem);
    }

    /* 코인 클릭 실패 PetCoinMng.CoinWaitRoutine()에서 */
    public void MissMoney()
    {
        missedMoney[0]++;
        missedMoney[1]++;

       // Debug.Log($"miss money for {missedMoney} times");
        IsMissionCleared(AMission.Type.MISS_MONEY, missedMoney);
    }

    /* 코인 클릭 PetCoinMng.GetCoin()에서 */
    public void FreeMoney()
    {
        freeMoney[0]++;
        freeMoney[1]++;

        //Debug.Log($"free money for {freeMoney} times");
        IsMissionCleared(AMission.Type.FREE_MONEY, freeMoney);
    }
    
    /* 유저 플레이 시간 1분 = 1 TimeMng.UserPlayTimeRoutine()에서 */
    public void PlayTime()
    {
        playTime[0]++;
        playTime[1]++;
        SeqPlayTime();

        //Debug.Log($"play for {playTime} minutes");
        IsMissionCleared(AMission.Type.PLAY_TIME, playTime);
    }

    /* 연속 플레이 시간 */
    private void SeqPlayTime()
    {
        seqPlayTime[0]++;
        seqPlayTime[1]++;

        //Debug.Log($"sequential play for {seqPlayTime} minutes");
        IsMissionCleared(AMission.Type.SEQUENTIAL_PLAY_TIME, seqPlayTime);
    }

    public void ResetSeqPlayTime()
    {
        seqPlayTime[0] = 0;
        seqPlayTime[1] = 0;
    }

    private MissionAlarmMng GetMissionAlarm()
    {
        if (!missionAlarmMng)
        {
            missionAlarmMng = GameObject.Find("MissionAlarm").GetComponent<MissionAlarmMng>();
        }

        return missionAlarmMng;
    }

    private ShowMissionMng GetShowMissionMng()
    {
        if (!showMissionMng)
        {
            showMissionMng = GameObject.Find("ShowMission").GetComponent<ShowMissionMng>();
        }

        return showMissionMng;
    }

    public void Reset()
    {
        acceptPet[1] = 0;
        declinePet[1] = 0;
        makeGround[1] = 0;
        destroyGround[1] = 0;
        zeroStar[1] = 0;
        oneStar[1] = 0;
        twoStar[1] = 0;
        threeStar[1] = 0;
        perfectStar[1] = 0;
        buyItem[1] = 0;
        giveItem[1] = 0;
        missedItem[1] = 0;
        missedMoney[1] = 0;
        freeMoney[1] = 0;
        playTime[1] = 0;
        seqPlayTime[1] = 0;

        foreach (string key in acceptThePet.Keys) acceptThePet[key][1] = 0;
        foreach (string key in buyTheItem.Keys) buyTheItem[key][1] = 0;
        foreach (string key in giveTheItem.Keys) giveTheItem[key][1] = 0;
    }

    public void IsMissionCleared(UserMission userMission)
    {
        AMission.Type missionType = (AMission.Type)Enum.Parse(typeof(AMission.Type), userMission.type);
        int[] counter = GetMissionCounter(missionType, userMission.conditionType);
        if (string.IsNullOrWhiteSpace(userMission.conditionType))
        {
            IsMissionCleared(missionType, counter);
        }
        else
        {
            IsMissionCleared(missionType, userMission.conditionType, counter);
        }
    }

    private int[] GetDefault(Dictionary<string, int[]> dict, string key)
    {
        if (!dict.ContainsKey(key))
        {
            dict.Add(key, new int[2] { 0, 0 });
        }
        return dict[key];
    }

    private int[] GetMissionCounter(AMission.Type missionType, string conditionType)
    {
        switch (missionType)
        {
            case AMission.Type.ACCEPT_THE_PET: return GetDefault(acceptThePet, conditionType);
            case AMission.Type.ACCEPT_PET: return acceptPet;
            case AMission.Type.DECLINE_PET: return declinePet;
            case AMission.Type.MAKE_GROUND: return makeGround;
            case AMission.Type.DESTROY_GROUND: return destroyGround;
            case AMission.Type.ZERO_STAR: return zeroStar;
            case AMission.Type.ONE_STAR: return oneStar;
            case AMission.Type.TWO_STAR: return twoStar;
            case AMission.Type.THREE_STAR: return threeStar;
            case AMission.Type.PERFECT_STAR: return perfectStar;
            case AMission.Type.BUY_ITEM: return buyItem;
            case AMission.Type.GIVE_ITEM: return giveItem;
            case AMission.Type.MISS_ITEM: return missedItem;
            case AMission.Type.FREE_MONEY: return freeMoney;
            case AMission.Type.MISS_MONEY: return missedMoney;
            case AMission.Type.PLAY_TIME: return playTime;
            case AMission.Type.SEQUENTIAL_PLAY_TIME: return seqPlayTime;
            case AMission.Type.BUY_THE_ITEM: return GetDefault(buyTheItem, conditionType);
            case AMission.Type.GIVE_THE_ITEM: return GetDefault(giveTheItem, conditionType);
            default: throw new Exception($"dont have log for type {missionType}");
        }
    }

    private void IsMissionCleared(AMission.Type missionType, int[] cnt)
    {
        AMission mission = Mission.Instance.IsCurMission(missionType);
        if (mission != null)
        {
            GetShowMissionMng().UpdateMission(mission);
            bool cleared = mission.refreshed ? mission.conditionCnt <= cnt[1] : mission.conditionCnt <= cnt[0]; 
            if (cleared) GetMissionAlarm().Open(mission);
        }
    }
    private void IsMissionCleared(AMission.Type missionType, string type, int[] cnt)
    {
        AMission mission = Mission.Instance.IsCurMission(type, missionType);
        if (mission != null)
        {
            GetShowMissionMng().UpdateMission(mission);
            bool cleared = mission.refreshed ? mission.conditionCnt <= cnt[1] : mission.conditionCnt <= cnt[0]; 
            if (cleared) GetMissionAlarm().Open(mission);
        }
    }

}
