﻿using UnityEngine;
using UnityEditor;
using System;

[Serializable]
public class SettingData: Singleton<SettingData>
{
    public bool musicOn;
    public bool effectOn;
    
    public SettingData()
    {
        Set(SaveLoadMng.LoadSetting());
    }

    public void Save()
    {
        SaveLoadMng.SaveSetting(this);
    }
    
    public void Set(SettingData saved)
    {
        if (saved == null)
        {
            musicOn = true;
            effectOn = true;
        }
        else
        {
            musicOn = saved.musicOn;
            effectOn = saved.effectOn;
        }
    }
}