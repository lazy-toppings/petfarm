﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;

[Serializable]
public class PetData
{
    public List<Pet> pets = new List<Pet>();

    public PetData()
    {
        pets = new List<Pet>();
    }

    public void AddPet(Pet p)
    {
        pets.Add(p);
        Data.Instance.Save();
    }

    public Pet GetPet(int id)
    {
        return pets.Find(p => p.id == id);
    }

    public void Replace(List<Pet> pet)
    {
        pets = pet;
        Data.Instance.Save();
    }

    public Pet GetNewPet(string p, int id, int dday)
    {
        return new Pet(id, p, dday);
    }

}