﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class Inventory
{
    public Dictionary<string, int> item = new Dictionary<string, int>();
    public Dictionary<string, Dictionary <string, List<MyPetItem>>> petAct = new Dictionary<string, Dictionary<string, List<MyPetItem>>>();
    // petAct[pettype][param] = List<items>

    public int land;
    public int money;

    [NonSerialized]
    private List<string> sortedItem;
    [NonSerialized]
    private ShowMoneyMng moneyMng;

    public Inventory()
    {
        land = 3;
        money = 2500;
    }

    public List<string> GetSortedItem()
    {
        if (sortedItem != null) return sortedItem;
        return Sort();
    }

    public List<string> Sort()
    {
        sortedItem = new List<string>();
        foreach (string i in item.Keys) sortedItem.Add(i);
        sortedItem = sortedItem.OrderBy(i => FarmConfig.Instance.ItemConfig(i).price).ToList();
        return sortedItem;
    }   

    public void AddLand()
    {
        land++;
    }

    public void ReduceLand()
    {
        land--;
    }

    public void AddItem(string i, int count)
    {
        if (item.ContainsKey(i))
        {
            item[i] += count;
        }
        else
        {
            item.Add(i, count);
            Sort();
        }
    }

    public void AddAllItem(int count)
    {
        foreach (string item in FarmConfig.Instance.Items())
        {
            AddItem(item, count);
        }
        Sort();
    }

    public int UseItemAndRemains(string i)
    {
        var ret = 0;
        if (item.ContainsKey(i))
        {
            ret = --item[i];
            if (item[i] == 0)
            {
                item.Remove(i);
                Sort();
            }
        }
        Data.Instance.Save();
        return ret;
    }

    public void AddMoney(int amount)
    {
        money += amount;
        GetMoneyMng().ShowMoney();
    }

    public MyPetItem HasAct(string pet, string param, int id)
    {
        if (!petAct.ContainsKey(pet)) return null;
        if (!petAct[pet].ContainsKey(param)) return null;
        return petAct[pet][param].Find(i => i.id == id);
    }

    public void AddPetAct(string pet, string param, int id)
    {
        MyPetItem item = new MyPetItem(id, true);
        if (!petAct.ContainsKey(pet)) petAct.Add(pet, new Dictionary<string, List<MyPetItem>>() { { param, new List<MyPetItem> { item } } });
        else if (!petAct[pet].ContainsKey(param)) petAct[pet].Add(param, new List<MyPetItem> { item });
        else petAct[pet][param].Add(item);
    }

    public void UseAct(string pet, string param, int id)
    {
        petAct[pet][param].Find(item => item.id == id).inUse = true;
    }

    public List<int> ActiveActs(string pet, string param)
    {
        List<int> ids = new List<int>();
        if (!petAct.ContainsKey(pet) || !petAct[pet].ContainsKey(param)) return ids;
        foreach (MyPetItem item in petAct[pet][param]) if (item.inUse) ids.Add(item.id);
        return ids;
    }

    private ShowMoneyMng GetMoneyMng()
    {
        if (!moneyMng)
        {
            moneyMng = GameObject.Find("Money").GetComponent<ShowMoneyMng>();
        }

        return moneyMng;
    }
}

[Serializable]
public class MyPetItem
{
    public int id;
    public bool inUse;

    public MyPetItem(int id, bool inUse)
    {
        this.id = id;
        this.inUse = inUse;
    }
}