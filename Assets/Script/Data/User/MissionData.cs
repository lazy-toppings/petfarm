﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;
using System.Linq;

[Serializable]
public class MissionData
{
    public int level;
    public List<UserMission> missions;

    [NonSerialized]
    private ShowMissionMng showMissionMng;

    public MissionData()
    {
        level = 0;
        missions = new List<UserMission>();
        Sync();
    }

    public void Sync()
    {
        List<AMission> m = Mission.Instance.MissionList(level);
       
        foreach (AMission mission in m)
        {
            missions.Add(UserMission.Of(mission, level));
        }
        Sort();
    }

    public bool SetComplete(AMission fulfilled)
    {
        UserMission mission = missions.Find(m => m.MatchAMission(fulfilled));
        if (mission.complete) return false;
        mission.complete = true;

        if (IsNextLevel())
        {
            level++;
            Sync();
            Data.Instance.logData.Reset();
            
            GetShowMissionMng().LoadMissions(true);
        }
        else Sort();
        return true;
    }

    public void SetGetReward(AMission fulfilled)
    {
        UserMission mission = missions.Find(m => m.MatchAMission(fulfilled));
        mission.getReward = true;
        Sort();
    }

    public void Sort()
    {
        missions = missions.OrderBy(m => m.getReward).ThenBy(m => m.complete).ToList<UserMission>();
    }

    public bool IsNew()
    {
        foreach (UserMission m in missions) if (m.complete && !m.getReward) return true;
        return false;
    }

    public bool IsNextLevel()
    {
        foreach (UserMission m in missions) if (!m.complete) return false;
        return true;
    }

    public List<UserMission> GetCurMissions()
    {
        return missions.FindAll(m => m.level == level);
    }

    public UserMission GetUserMission(AMission mission)
    {
        return missions.Find(m => m.MatchAMission(mission));
    }

    private ShowMissionMng GetShowMissionMng()
    {
        if (!showMissionMng)
        {
            showMissionMng = GameObject.Find("ShowMission").GetComponent<ShowMissionMng>();
        }

        return showMissionMng;
    }
}

[Serializable]
public class UserMission
{
    public string type;
    public string conditionType;
    public int id;
    public int level;
    public bool complete;
    public bool getReward;
    public bool refreshed;

    public UserMission(string type, string conditionType, int id, int level, bool refreshed)
    {
        this.type = type;
        this.conditionType = conditionType;
        this.id = id;
        this.level = level;
        complete = false;
        getReward = false;
        this.refreshed = refreshed;
    }

    public static UserMission Of(AMission mission, int level)
    {
        return new UserMission(mission.type.ToString(), mission.conditionType, mission.id, level, mission.refreshed);
    }

    public bool MatchAMission(AMission mission)
    {
        return type == mission.type.ToString() && id == mission.id;
    }

    public int MatchLog()
    {
        LogData log = Data.Instance.logData;
        int index = refreshed ? 1 : 0;
        switch (type)
        {
            case "ACCEPT_THE_PET": return log.acceptThePet.ContainsKey(conditionType) ? log.acceptThePet[conditionType][index] : 0;
            case "ACCEPT_PET": return log.acceptPet[index];
            case "DECLINE_PET": return log.declinePet[index];
            case "MAKE_GROUND": return log.makeGround[index];
            case "DESTROY_GROUND": return log.destroyGround[index];
            case "ZERO_STAR": return log.zeroStar[index];
            case "ONE_STAR": return log.oneStar[index];
            case "TWO_STAR": return log.twoStar[index];
            case "THREE_STAR": return log.threeStar[index];
            case "PERFECT_STAR": return log.perfectStar[index];
            case "BUY_ITEM": return log.buyItem[index];
            case "GIVE_ITEM": return log.giveItem[index];
            case "MISS_ITEM": return log.missedItem[index];
            case "FREE_MONEY": return log.freeMoney[index];
            case "MISS_MONEY": return log.missedMoney[index];
            case "PLAY_TIME": return log.playTime[index];
            case "SEQUENTIAL_PLAY_TIME": return log.seqPlayTime[index];
            case "BUY_THE_ITEM": return log.buyTheItem.ContainsKey(conditionType) ? log.buyTheItem[conditionType][index] : 0;
            case "GIVE_THE_ITEM": return log.giveTheItem.ContainsKey(conditionType) ? log.giveTheItem[conditionType][index] : 0;
            default: throw new Exception($"dont have log for type {type}");
        }
    }
}