﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

[Serializable]
public enum Buildings
{
    Empty,
    House,
    Bank,
    Factory,
    Desert,
    Beach,
    Waterfall,
    Forest,
    Destory,
}

[Serializable]
public class GroundData
{
    public Buildings[,] gBuildings;
    public int[,] gPets;

    public GroundData()
    {
        gBuildings = new Buildings[5, 3]
        {
            { Buildings.Factory, Buildings.House, Buildings.Bank },
            { Buildings.Empty, Buildings.Empty, Buildings.Empty },
            { Buildings.Empty, Buildings.Empty, Buildings.Empty },
            { Buildings.Empty, Buildings.Empty, Buildings.Empty },
            { Buildings.Empty, Buildings.Empty, Buildings.Empty }
        };
        gPets = new int[5, 3]
        {
            { -1, -1, -1 },
            { -1, -1, -1 },
            { -1, -1, -1 },
            { -1, -1, -1 },
            { -1, -1, -1 }
        };
    }

    public void BuildBuilding(int x, int y, Buildings b)
    {
        gBuildings[y, x] = b;
        Data.Instance.Save();
    }
    
    public void RemoveBuilding(int x, int y)
    {
        gBuildings[y, x] = Buildings.Empty;
        Data.Instance.Save();
    }

    public void RemovePet(int id)
    {
        for (int y = 0; y < 5; ++y) for (int x = 0; x < 3; ++x)
        {
            if (gPets[y, x] == id) gPets[y, x] = -1;
        }
        Data.Instance.Save();
    }

    public void BuildPet(int x, int y, int p)
    {
        gPets[y, x] = p;
        Data.Instance.Save();
    }

    public Buildings GetBuilding(int x, int y)
    {
        return gBuildings[y, x];
    }

    public bool ExistExtra()
    {
        return OccupiedCount() < Data.Instance.inventory.land;
    }
    
    public int[] GetEmptySpace()
    {
        for (int y = 0; y < 5; ++y)
        {
            for (int x = 0; x < 3; ++x)
            {
                if (gBuildings[y, x] == Buildings.Empty)
                    return new int[2] {x, y};
            }
        }
        return new int[2] { -1, -1 };
    }

    public int[] GetEmptyBuilding()
    {
        for (int y = 1; y < 5; ++y)
        {
            for (int x = 0; x < 3; ++x)
            {
                if (gBuildings[y, x] != Buildings.Empty &&
                    gPets[y, x] == -1)
                    return new int[2] { x, y };
            }
        }
        return new int[2] { -1, -1 };
    }

    public int[] GetEmptyBuilding(List<Buildings> buildings)
    {
        for (int y = 0; y < 5; ++y)
        {
            for (int x = 0; x < 3; ++x)
            {
                if (buildings.Contains(gBuildings[y, x])&&
                    gPets[y, x] == -1)
                    return new int[2] { x, y };
            }
        }
        return new int[2] { -1, -1 };
    }

    public int[] GetPetXY(int id)
    {
        for (int y = 0; y < 5; ++y)
        {
            for (int x = 0; x < 3; ++x)
            {
                if (gPets[y, x] == id)
                    return new int[2] { x, y };
            }
        }
        return new int[2] { -1, -1 };
    }

    public int Price()
    {
        int cnt = 0;
        for (int y = 0; y < 5; ++y)
        {
            for (int x = 0; x < 3; ++x)
            {
                if (gBuildings[y, x] == Buildings.Empty) cnt++;
            }
        }
        
        return 250 * (13 - cnt) * (12 - cnt) + 500;
    }

    public int OccupiedCount()
    {
        var cnt = 0;
        for (int y = 1; y < 5; ++y)
        {
            for (int x = 0; x < 3; ++x)
            {
                if (gBuildings[y, x] != Buildings.Empty)
                    cnt += 1;
            }
        }
        return cnt;
    }
}