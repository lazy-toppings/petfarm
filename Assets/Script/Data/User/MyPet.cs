﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

[Serializable]
public class MyPet
{
    //public List<ChainPet> pets;

    public Dictionary<string, MyPetInfo> pets;
    public Dictionary<string, MyPetWork> works;

    [NonSerialized]
    private WalkingPet walkingPet;
    [NonSerialized]
    private List<string> sortedPet;

    public MyPet()
    {
        pets = new Dictionary<string, MyPetInfo>();
        works = new Dictionary<string, MyPetWork>();
    }

    public string GetRandom()
    {
        if (pets.Count == 0) return null;
        List<string> myPets = pets.Keys.ToList();
        return myPets[UnityEngine.Random.Range(0, myPets.Count)];
    }
    
    public void Add(ChainPet pet)
    {
        if (pets.ContainsKey(pet.petType))
        {
            pets[pet.petType].count++;
        }
        else
        {
            Data.Instance.inventory.AddLand();
            pets.Add(pet.petType, new MyPetInfo(pet.petType, 1, 0));
            works.Add(pet.petType, new MyPetWork());
            Sort();
        }
        pets[pet.petType].pass++;
        if (pets.Count == 1) GetWalkingPet().GeneratePet();
    }

    public void Add(string pet, int count)
    {
        if (pets.ContainsKey(pet)) return;
        
        Data.Instance.inventory.AddLand();
        pets.Add(pet, new MyPetInfo(pet, count, 0));
        works.Add(pet, new MyPetWork());
        Sort();
        
        if (pets.Count == 1) GetWalkingPet().GeneratePet();
    }

    public void Remove(string pet)
    {
        if (pets[pet].pass == 0)
        {
            pets.Remove(pet);
            works.Remove(pet);
            Data.Instance.inventory.ReduceLand();
            Sort();
        }
        else
        {
            pets[pet].count = pets[pet].pass;
        }
    }

    private WalkingPet GetWalkingPet()
    {
        if (!walkingPet)
        {
            try
            {
                walkingPet = GameObject.Find("MyPet").GetComponent<WalkingPet>();
            }
            catch (Exception e)
            {
                
            }
        }

        return walkingPet;
    }

    public bool IsMine(string pet)
    {
        return pets.ContainsKey(pet);
    }

    public List<string> PetInSortedOrder()
    {
        if (sortedPet != null) return sortedPet;

        Sort();
        return sortedPet;
    }

    private void Sort()
    {
        sortedPet = new List<string>();
        
        foreach (string p in pets.Keys) sortedPet.Add(p);
        foreach (string p in FarmConfig.Instance.Pets()) if (!sortedPet.Contains(p)) sortedPet.Add(p);
    }
    
    public void LevelUp(string p)
    {
        pets[p].level++;
    }

    public void WorkDone(string p)
    {
        works[p].isWork = false;
        works[p].isReward = true;
    }

    public void GetReward(string p)
    {
        works[p].isReward = false;
    }

    public DateTime SendWork(string p)
    {
        works[p].isWork = true;
        works[p].workTime = DateTime.Now;
        return works[p].workTime;
    }

    public bool IsNewReward()
    {
        foreach (MyPetWork w in works.Values) if (w.isReward) return true;
        return false;
    }
}

[Serializable]
public class MyPetInfo
{
    public string type;
    public int count;
    public int level;
    public int pass;

    public MyPetInfo(string type, int count, int level)
    {
        this.type = type;
        this.count = count;
        this.level = level;
    }
}
[Serializable]
public class MyPetWork
{
    public bool isWork;
    public bool isReward;
    public DateTime workTime;

    public MyPetWork()
    {
        isWork = false;
        isReward = false;
        workTime = new DateTime();
    }
}

[Serializable]
public class ChainPet
{
    public string assetId;
    public string petType;

    public ChainPet(string assetId, string petType)
    {
        this.assetId = assetId;
        this.petType = petType;
    }
}

