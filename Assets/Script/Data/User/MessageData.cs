﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;

[Serializable]
public class MessageData
{
    private readonly int[] intervalByLevel = new int[] { 2, 1, 1, 1, 1, 0, 0, 0, 0, 0 };

    public List<Message> messages = new List<Message>();
    public int globalId;
    public int nextDate;

    public MessageData()
    {
        globalId = 0;
        nextDate = 0;
        messages = new List<Message>();
    }

    public int GetId()
    {
        return globalId++;
    }

    public int NextDate => nextDate;

    public void AddMessage(Message p, bool updateDate = true)
    {
        messages.Add(p);
        var level = Data.Instance.playData.level;
        if (updateDate) nextDate = p.startDate - 1 + intervalByLevel[level];
        Data.Instance.Save();
    }

    public List<Message> IsNew()
    {
        return messages.FindAll(m => m.status == "Waiting");
    }

    public Message GetMessage(int id)
    {
        return messages.Find(m => m.id == id);
    }

    public void ChangeStatus(int id, string status)
    {
        messages[id].status = status;
    }

    public List<Message> GetActiveMessage()
    {
        return messages.FindAll(m => m.status == "Accepted" || m.status == "Waiting");
    }
}