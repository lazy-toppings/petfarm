﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

[Serializable]
public class Data : Singleton<Data>
{
    public string id;
    public GroundData groundData;
    public Inventory inventory;
    public LogData logData;
    public MessageData messageData;
    public MissionData missionData;
    public MyPet myPet;
    public PetData petData;
    public PlayData playData;
    
    public Data()
    {
        Set(SaveLoadMng.LoadData());
    }

    public void Load(string jsonStr)
    {
        var jsonConfig = JsonUtility.FromJson<Data>(jsonStr);
        id = jsonConfig.id;
        groundData = jsonConfig.groundData;
        inventory = jsonConfig.inventory;
        logData = jsonConfig.logData;
        messageData = jsonConfig.messageData;
        missionData = jsonConfig.missionData;
        myPet = jsonConfig.myPet;
        petData = jsonConfig.petData;
        playData = jsonConfig.playData;
    }

    public string ToJson()
    {
        return JsonConvert.SerializeObject(this);
    }
    
    
    public void Set(Data saved)
    {
        if (saved == null || !saved.playData.tutoDone)
        {
            // id = "44uqfatds1br";
            groundData = new GroundData();
            inventory = new Inventory();
            logData = new LogData();
            missionData = new MissionData();
            myPet = new MyPet();
            petData = new PetData();
            playData = new PlayData();
            messageData = new MessageData();
        }
        else
        {
            id = saved.id;
            groundData = saved.groundData;
            inventory = saved.inventory;
            logData = saved.logData;
            messageData = saved.messageData;
            missionData = saved.missionData;
            myPet = saved.myPet;
            petData = saved.petData;
            playData = saved.playData;
        }
    }

    public void Save()
    {
        SaveLoadMng.SaveData(this);
    }
}