﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

[Serializable]
public class Pet
{
    public int id;
    public string kinds;
    public int dday;
    public int wanted;
    public int got;

    [NonSerialized]
    private PetConfig config;
    [NonSerialized]
    private Random rnd;

    public List<Buildings> Fences => FarmConfig.Instance.PetConfig(kinds).fences;
    public string Name => FarmConfig.Instance.PetConfig(kinds).name;

    public Pet(int id, string kinds, int dday)
    {
        this.id = id;
        this.kinds = kinds;
        this.dday = dday;
    }

    public void Want()
    {
        wanted++;
    }

    public void GetItem(string itemKind)
    {
        got++;
        Data.Instance.playData.AddExp(Item.RewardExp(itemKind));
    }

    public bool CanReside(Buildings fence)
    {
        return GetConfig().fences.Contains(fence);
    }

    public RankEnum Reward(Inventory inventory, PlayData playData)
    {
        var rank = Rank();
        var factor = RewardFactor(rank);
        return rank;
    }

    public int Reward(RankEnum rank)
    {
        var factor = RewardFactor(rank);
        return (int)(got * 40 * factor);
    }

    private RankEnum Rank()
    {
        var score = got * 100.0 / wanted;
        if (score >= 100) return RankEnum.S;
        if (score >= 80) return RankEnum.A;
        if (score >= 60) return RankEnum.B;
        if (score >= 30) return RankEnum.C;
        return RankEnum.F;
    }

    private double RewardFactor(RankEnum rank)
    {
        switch (rank)
        {
            case RankEnum.S: return 1.4;
            case RankEnum.A: return 1.2;
            case RankEnum.B: return 1.1;
            case RankEnum.C: return 1;
            case RankEnum.F: return 0;
            default: return 1;
        }
    }

    private PetConfig GetConfig()
    {
        return config ?? (config = FarmConfig.Instance.PetConfig(kinds));
    }

    private int GetRandom(Boundary boundary)
    {
        if (rnd == null) rnd = new Random();
        return -rnd.Next(boundary.min, boundary.max + 1);
    }

}