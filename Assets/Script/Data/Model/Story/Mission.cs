﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

public class Mission : Singleton<Mission>
{
    private readonly string filepath = "mission.json";

    private Dictionary<int, List<AMission>> missionDict;
        
    public Mission()
    {
        Load();
    }

    public List<AMission> MissionList(int level)
    {
        if (missionDict.ContainsKey(level)) return missionDict[level];
        
        return null;
    }

    public AMission Of(UserMission mission)
    {
        int level = mission.level;
        return missionDict[level].Find(aMission => aMission.id == mission.id);
    }
    /*
    public AMission Fulfilled(AMission.Type type, int amount)
    {
        int level = Data.Instance.missionData.level;
        return missionDict[level].Find(mission => mission.type == type && mission.conditionCnt <= amount);
    }
    */
    public AMission IsCurMission(AMission.Type type)
    {
        int level = Data.Instance.missionData.level;
        return missionDict[level].Find(mission => mission.type == type);
    }
    /*
    public AMission Fulfilled(string petOrItem, AMission.Type type, int amount)
    {
        int level = Data.Instance.missionData.level;
        return missionDict[level].Find(mission => mission.type == type && mission.conditionType == petOrItem && mission.conditionCnt <= amount);
    }
    */
    public AMission IsCurMission(string petOrItem, AMission.Type type)
    {
        int level = Data.Instance.missionData.level;
        return missionDict[level].Find(mission => mission.type == type && mission.conditionType == petOrItem);
    }

    public int MissionCnt()
    {
        return missionDict.Count;
    }

    private void Load()
    {
        string path = Path.Combine(Application.streamingAssetsPath, filepath);
#if UNITY_EDITOR || UNITY_IOS
        var bytes = File.ReadAllBytes(path);
        var jsonStr = Encoding.UTF8.GetString(bytes, 3, bytes.Length - 3);

#elif UNITY_ANDROID
            WWW reader = new WWW (path);
            while (!reader.isDone) {
            }
            var jsonStr = Encoding.UTF8.GetString(reader.bytes, 3, reader.bytes.Length - 3);
#endif
        var jsonConfig = JsonConvert.DeserializeObject<MissionWrapper>(jsonStr);
        missionDict = jsonConfig.mission;
    }

    [Serializable]
    public class MissionWrapper
    {
        public Dictionary<int, List<AMission>> mission;
    }

}