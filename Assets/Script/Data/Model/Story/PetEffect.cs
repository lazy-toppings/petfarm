﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;

public class PetEffect : Singleton<PetEffect>
{
    private readonly string filepath = "peteffect.json";

    public List<int> petTier;
    public List<PetWork> work;


    public PetEffect()
    {
        Load();
    }

    private void Load()
    {
        string path = Path.Combine(Application.streamingAssetsPath, filepath);
#if UNITY_EDITOR || UNITY_IOS
        var bytes = File.ReadAllBytes(path);
        var jsonStr = Encoding.UTF8.GetString(bytes, 3, bytes.Length - 3);

#elif UNITY_ANDROID
            WWW reader = new WWW (path);
            while (!reader.isDone) {
            }
            var jsonStr = Encoding.UTF8.GetString(reader.bytes, 3, reader.bytes.Length - 3);
#endif
        var jsonConfig = JsonConvert.DeserializeObject<PetEffectWrapper>(jsonStr);
        petTier = jsonConfig.petTier;
        work = jsonConfig.work;
    }

    [Serializable]
    public class PetEffectWrapper
    {
        public List<int> petTier;
        public List<PetWork> work;
    }
}

[Serializable]
public class PetWork
{
    public string type;
    public List<int> cost;
    public List<int> earn;
    public List<int> time;
}
