﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Newtonsoft.Json;

public class PetItem : Singleton<PetItem>
{
    private string filepath = "petitem.json";

    public Dictionary<string, List<APetItem>> skin;
    public Dictionary<string, List<APetItem>> act;

    public PetItem()
    {
        Load();
    }

    private void Load()
    {
        string path = Path.Combine(Application.streamingAssetsPath, filepath);
#if UNITY_EDITOR || UNITY_IOS
        var bytes = File.ReadAllBytes(path);
        var jsonStr = Encoding.UTF8.GetString(bytes, 3, bytes.Length - 3);

#elif UNITY_ANDROID
            WWW reader = new WWW (path);
            while (!reader.isDone) {
            }
            var jsonStr = Encoding.UTF8.GetString(reader.bytes, 3, reader.bytes.Length - 3);
#endif
        var jsonConfig = JsonConvert.DeserializeObject<PetItemWrapper>(jsonStr);
        skin = jsonConfig.skin;
        act = jsonConfig.act;
    }

    [Serializable]
    public class PetItemWrapper
    {
        public Dictionary<string, List<APetItem>> skin;
        public Dictionary<string, List<APetItem>> act;
    }
}