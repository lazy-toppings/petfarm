﻿using UnityEngine;
using UnityEditor;
using System;

[Serializable]
public class AStory
{
    public string image;
    public string sound;
    public string topText;
    public string bottomText;
}