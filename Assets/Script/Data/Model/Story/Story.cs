﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System;

public class Story : Singleton<Story>
{
    private string filepath = "story.json";

    private Dictionary<int, List<AStory>> story = new Dictionary<int, List<AStory>>();

    public Story()
    {
        Load();
    }
    
    public List<AStory> GetStory(int idx)
    {
        return story[idx];
    }

    private void Load()
    {
        string path = Path.Combine(Application.streamingAssetsPath, filepath);
#if UNITY_EDITOR || UNITY_IOS
        var jsonStr = File.ReadAllText(path);

#elif UNITY_ANDROID
            WWW reader = new WWW (path);
            while (!reader.isDone) {
            }
            var jsonStr = reader.text;
#endif
        var jsonConfig = JsonUtility.FromJson<StoryWrapper>(jsonStr);
        story.Add(0, jsonConfig.story0);
    }

    [Serializable]
    public class StoryWrapper
    {
        public List<AStory> story0;
    }

}