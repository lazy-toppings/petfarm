﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class Tutorial : Singleton<Tutorial>
{
    private string filepath = "tutorial.json";

    public List<ATutorial> tutorial;

    public Tutorial()
    {
        Load();
    }

    private void Load()
    {
        string path = Path.Combine(Application.streamingAssetsPath, filepath);
#if UNITY_EDITOR || UNITY_IOS
        var bytes = File.ReadAllBytes(path);
        var jsonStr = Encoding.UTF8.GetString(bytes, 3, bytes.Length - 3);

#elif UNITY_ANDROID
            WWW reader = new WWW (path);
            while (!reader.isDone) {
            }
            var jsonStr = Encoding.UTF8.GetString(reader.bytes, 3, reader.bytes.Length - 3);
#endif
        var jsonConfig = JsonUtility.FromJson<TutorialWrapper>(jsonStr);
        tutorial = jsonConfig.tutorial;
    }

    [Serializable]
    public class TutorialWrapper
    {
        public List<ATutorial> tutorial;
    }
}
