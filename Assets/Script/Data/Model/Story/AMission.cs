﻿using UnityEngine;
using UnityEditor;
using System;

[Serializable]
public class AMission
{
    public Type type;
    public int id;
    public int level;
    public string condition;
    public string conditionType; // item이나 pet
    public int conditionCnt;
    public string rewardType;
    public int rewardAmount;
    public bool refreshed;

    public enum Type
    {
        ACCEPT_THE_PET, ACCEPT_PET, DECLINE_PET, MAKE_GROUND,
        DESTROY_GROUND, ZERO_STAR, ONE_STAR, TWO_STAR, THREE_STAR,
        PERFECT_STAR, BUY_ITEM, GIVE_ITEM, MISS_ITEM, FREE_MONEY,
        MISS_MONEY, PLAY_TIME, SEQUENTIAL_PLAY_TIME, BUY_THE_ITEM,
        GIVE_THE_ITEM
    }

    public string Name => $"mission-{id}";
}