﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class ATutorial
{
    public string progress;
    public string content;
    public bool events;
}
