﻿using UnityEngine;
using UnityEditor;
using System;

[Serializable]
public class APetItem
{
    public int id;
    public int price;
    public string info;
    public string param;
}