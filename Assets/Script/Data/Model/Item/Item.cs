﻿
using UnityEngine;
using UnityEditor;
using System;


[Serializable]
public class Item
{
    private string kinds;

    public Item(string kinds)
    {
        this.kinds = kinds;
    }

    public static int RewardExp(string kinds)
    {
        var config = FarmConfig.Instance.ItemConfig(kinds);
        return (int)Math.Round(Math.Pow(config.price, 1.25) / 3);
    }

    public string Kinds { get => kinds; set => kinds = value; }
   
    public int Price => FarmConfig.Instance.ItemConfig(kinds).price;
}
