﻿using UnityEngine;
using UnityEditor;
using System;

[Serializable]
public class Box
{
    public int id;
    public bool opened;
    public string petType;
}