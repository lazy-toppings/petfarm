﻿using UnityEngine;
using System.Collections;
using System;
using Random = UnityEngine.Random;

[Serializable]
public class Message
{
    public int id;
    public string status;
    public int startDate;
    public int endDate;
    public string content;
    public string petType;
    public int cost;

    public Message(int id, int startDate, int period, string content, string petType)
    {
        this.id = id;
        status = "Waiting";
        this.startDate = startDate;
        endDate = startDate + period;
        this.content = content;
        this.petType = petType;
        int petLevel = FarmConfig.Instance.PetConfig(petType).level;
        cost = (int)(period * Random.Range(30, 101) * (0.25f * petLevel + 0.75f));
    }

    public void Done()
    {
        status = "Done";
        Data.Instance.Save();
    }
}