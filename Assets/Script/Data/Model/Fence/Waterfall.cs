﻿using UnityEngine;
using UnityEditor;

public class Waterfall : Fence
{
    public Waterfall() : base(Buildings.Waterfall)
    {
        Level = 0;
    }
}