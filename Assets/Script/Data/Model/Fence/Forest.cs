﻿using UnityEngine;
using UnityEditor;

public class Forest : Fence
{
    public Forest() : base(Buildings.Forest)
    {
        Level = 0;
    }
}