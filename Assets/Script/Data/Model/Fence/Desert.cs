﻿using UnityEngine;
using UnityEditor;
using System;

public class Desert : Fence
{
    public Desert() : base(Buildings.Desert)
    {
        Level = 0;
    }
}