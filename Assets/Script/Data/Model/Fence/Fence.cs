﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;

public abstract class Fence
{
    private int level;
    private Buildings kinds;

    protected Fence(Buildings kinds)
    {
        this.kinds = kinds;
    }

    public int Level { get => level; set => level = value; }
    public Buildings Kinds => kinds;
}

public class FenceFactory
{
    public static Fence Create(Buildings type)
    {
        switch (type)
        {
            case Buildings.Desert:
                return new Desert();
            case Buildings.Beach:
                return new Beach();
            case Buildings.Waterfall:
                return new Waterfall();
            case Buildings.Forest:
                return new Forest();
            default:
                return null;
        }
    }

    public static List<Fence> GetFences()
    {
        List<Fence> fences = new List<Fence>();
        foreach (Buildings b in Enum.GetValues(typeof(Buildings)))
        {
            Fence f = Create(b);
            if (f != null)  fences.Add(f);
        }
        return fences;
    }
}