﻿using UnityEngine;
using UnityEditor;

public class Beach : Fence
{
    public Beach() : base(Buildings.Beach)
    {
        Level = 0;
    }
}