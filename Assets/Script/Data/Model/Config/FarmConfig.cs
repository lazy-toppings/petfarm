using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;


[Serializable]
public class FarmConfig : Singleton<FarmConfig>
{
    private readonly string filepath = "config.json";
    public List<int> landExpand;
    private Dictionary<string, PetConfig> petConfigs = new Dictionary<string, PetConfig>();
    private Dictionary<string, ItemConfig> itemConfigs = new Dictionary<string, ItemConfig>();
    private Dictionary<Buildings, FenceConfig> fenceConfigs = new Dictionary<Buildings, FenceConfig>();
    private LevelConfig levelConfig = new LevelConfig();
    private List<string> items;
    private List<string> pets;
    private List<Buildings> fences;

    public FarmConfig()
    {
        Load();
    }

    public PetConfig PetConfig(string type)
    {
        return petConfigs[type];
    }

    public bool AvailablePetName(string name)
    {
        foreach (PetConfig config in petConfigs.Values)
        {
            if (config.name == name) return true;
        }

        return false;
    }

    public string PetTypeFromName(string name)
    {
        foreach (PetConfig config in petConfigs.Values)
        {
            if (config.name == name) return config.type;
        }

        return null;
    }

    public ItemConfig ItemConfig(string type)
    {
        return itemConfigs[type];
    }

    public FenceConfig FenceConfig(Buildings type)
    {
        return fenceConfigs[type];
    }

    public List<string> Items()
    {
        return items ?? (items = itemConfigs.Keys.ToList());
    }
   

    public int AllItemPrice()
    {
        int price = 0;
        foreach (KeyValuePair<string, ItemConfig> pair in itemConfigs)
        {
            price += pair.Value.price;
        }
        return price;
    }

    public List<string> Pets()
    {
        return pets ?? (pets = petConfigs.Keys.ToList());
    }

    public List<Buildings> Fences()
    {
        return fences ?? (fences = fenceConfigs.Keys.ToList());
    }

    public LevelConfig LevelConfig => levelConfig;
    
    
    private void Load()
    {
        string path = Path.Combine(Application.streamingAssetsPath, filepath);
#if UNITY_EDITOR || UNITY_IOS
        var bytes = File.ReadAllBytes(path);
        var jsonStr = Encoding.UTF8.GetString(bytes);

#elif UNITY_ANDROID
            WWW reader = new WWW (path);
            while (!reader.isDone) {
            }
            var jsonStr = Encoding.UTF8.GetString(reader.bytes);
#endif
        //Debug.Log(jsonStr);
        var jsonConfig = JsonUtility.FromJson<ConfigWrapper>(jsonStr);
        foreach (var config in jsonConfig.fenceList)
        {
            fenceConfigs.Add(config.type, config);
            //Debug.Log(config);
        }
        foreach (var config in jsonConfig.itemList)
        {
            itemConfigs.Add(config.type, config);
          //  Debug.Log(config);
        }
        foreach (var config in jsonConfig.petList)
        {
            petConfigs.Add(config.type, config);
           // Debug.Log(config);
        }

        landExpand = jsonConfig.landExpand;
        levelConfig = jsonConfig.level;
      //  Debug.Log(jsonConfig.level);
    }

    [Serializable]
    public class ConfigWrapper
    {
        public List<int> landExpand;
        public LevelConfig level;
        public List<ItemConfig> itemList;
        public List<PetConfig> petList;
        public List<FenceConfig> fenceList;
    }
}