using System;

[Serializable]
public class Boundary
{
    public int min;
    public int max;
    private Random rnd = new Random();

    public Boundary(int a, int b)
    {
        min = Math.Min(a, b);
        max = Math.Max(a, b);
    }

    public override string ToString()
    {
        return $"{min}~{max}";
    }

    public int GetRandom()
    {
        if (rnd == null) rnd = new Random();
        return rnd.Next(min, max + 1);
    }
}