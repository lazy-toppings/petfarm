using System;
using System.Collections.Generic;

[Serializable]
public class FenceConfig
{
    public Buildings type;

    public override string ToString()
    {
        return $"{type.ToString()}";
    }
}