using System;
using System.Collections.Generic;

[Serializable]
public class LevelConfig
{
    public List<int> exp;

    public override string ToString()
    {
        return $"level => [{String.Join(",", exp.ToArray())}]";
    }
}