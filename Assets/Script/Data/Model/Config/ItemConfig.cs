using System;

[Serializable]
public class ItemConfig
{
    public string type;
    public int price;

    public override string ToString()
    {
        return $"{type} -> p: {price}";
    }
}