using System;
using System.Collections.Generic;

[Serializable]
public class PetConfig
{
    public string type;
    public string name;
    public List<Buildings> fences;
    public int exp;
    public int reward;
    public int level;
    public string chain;

    public override string ToString()
    {
        return
            $"{type} -> " +
            $"fs: {String.Join("~", fences.ToArray())}, " +
            $"exp: {exp}, reward: {reward}, level: {level}, chain: {chain}";
    }
}