﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class ShowMissionMng : MonoBehaviour
{
    private List<UserMission> missions;

    void Start()
    {
        LoadMissions(true);
    }

    public void UpdateMission(AMission mission)
    {
        UserMission userMission = Data.Instance.missionData.GetUserMission(mission);
        if (userMission.complete) return;
        
        foreach (Transform t in transform)
        {
            if (t.name == mission.Name)
            {
                SetMissionInfo(t.gameObject, mission, userMission);
            }
        }
    }

    public void LoadMissions(bool nextLevel = false)
    {
        Close();
        if (nextLevel) missions = Data.Instance.missionData.GetCurMissions();
        foreach (UserMission m in missions)
        {
            AMission aMission = Mission.Instance.Of(m);
            GameObject g = Instantiate(Resources.Load("Prefabs/UI/Mission/MissionText")) as GameObject;
            g.name = aMission.Name;
            Data.Instance.logData.IsMissionCleared(m);
            SetMissionInfo(g, aMission, m);
            g.transform.SetParent(transform);
        }
        StartCoroutine(WaitForNextFrame());
    }

    IEnumerator WaitForNextFrame()
    {
        yield return 0;

        float y = -20;
        for (int i = 0; i < transform.childCount; ++i)
        {
            Transform child = transform.GetChild(i);
            if (!child.gameObject.activeSelf) continue;
            child.localPosition = new Vector2(59, y);
            y -= (child.GetComponent<RectTransform>().sizeDelta.y + 7);
        }

        transform.GetComponent<RectTransform>().sizeDelta = new Vector2(417.5f, -y+20);
    }

    private void Close()
    {
        for (int i = 0; i < transform.childCount; ++i)
            transform.GetChild(i).gameObject.SetActive(false);
    }

    private void SetMissionInfo(GameObject g, AMission aMission, UserMission uMission)
    {
        int logCnt = Math.Min(uMission.MatchLog(), aMission.conditionCnt);
        g.GetComponent<Text>().text = $"* {aMission.condition} ({logCnt}/{aMission.conditionCnt})";
        if (logCnt == aMission.conditionCnt) g.GetComponent<Text>().color = new Color(0.972549f, 0.7647059f, 0.07843138f);
    }
}
