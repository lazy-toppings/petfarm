﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowMoneyMng : MonoBehaviour
{
    public Text moneyText;

    private void Start()
    {
        ShowMoney();
    }

    public void ShowMoney()
    {
        moneyText.text = Data.Instance.inventory.money.ToString();
    }
}
