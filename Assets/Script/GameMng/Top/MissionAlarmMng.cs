﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MissionAlarmMng : MonoBehaviour
{
    public MissionMng missionMng;

    public GameObject alarm;
    public Text missionText;
    
    public void Open(AMission mission)
    {
        if (Data.Instance.missionData.SetComplete(mission))
        {
            alarm.SetActive(true);
            missionText.text = $"Mission clear: {mission.condition}";

            missionMng.SetNew();

            StartCoroutine(GoneRoutine());
        }
    }

    IEnumerator GoneRoutine()
    {
        yield return new WaitForSeconds(3);
        alarm.SetActive(false);
    }
}
