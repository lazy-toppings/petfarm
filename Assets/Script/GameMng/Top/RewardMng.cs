﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.U2D;
using UnityEngine.UI;

public class RewardMng : MonoBehaviour
{
    public SpriteAtlas petAtlas;
    
    private Transform yellowStar;
    private Image petImg;
    private Text starInfo;
    private Text rewardText;
    private Text extraText;
    private Text totalText;
    private Text dayText;
    
    private Queue<Tuple<Pet, RankEnum>> q = new Queue<Tuple<Pet, RankEnum>>();

    private void LoadObjects()
    {
        yellowStar = transform.Find("Star").Find("Yellow");
        petImg = transform.Find("PetImg").GetComponent<Image>();
        Transform infos = transform.Find("Infos");
        starInfo = infos.Find("StarInfo").GetComponent<Text>();
        rewardText = infos.Find("RewardText").GetComponent<Text>();
        extraText = infos.Find("ExtraText").GetComponent<Text>();
        totalText = infos.Find("TotalText").GetComponent<Text>();
        dayText = infos.Find("DayText").GetComponent<Text>();
    }

    public void AddReward(Pet p, RankEnum rank)
    {
        q.Enqueue(new Tuple<Pet, RankEnum>(p, rank));
    }

    public void IsClicked()
    {
        if (q.Count == 0) GetComponent<ClickGone>().Gone();
        else ShowReward();
    }

    public void ShowReward()
    {
        if (yellowStar == null)
        {
            LoadObjects();
        }
        
        gameObject.SetActive(true);
        GetComponent<AudioSource>().Play();

        Tuple<Pet, RankEnum> tuple = q.Dequeue();
        SetInfos(tuple.Item1, tuple.Item2);
    }

    private void SetInfos(Pet p, RankEnum rank)
    {
        Data.Instance.logData.SetStar(rank);
        int starIdx;
        switch (rank)
        {
            case RankEnum.A: case RankEnum.S: starIdx = 3; break;
            case RankEnum.B: starIdx = 2; break;
            case RankEnum.C: starIdx = 1; break;
            default: starIdx = 0; break;
        }

        if (rank == RankEnum.S) starInfo.text = "Perfect!";
        else if (rank == RankEnum.F) starInfo.text = "Failure...";
        else starInfo.text = "";

        for (int i = 0; i < 3; ++i)
        {
            yellowStar.GetChild(i).gameObject.SetActive(i < starIdx);
        }

        Message msg = Data.Instance.messageData.GetMessage(p.id);
        int cReward = msg.cost;
        int reward = p.Reward(rank);

        rewardText.text = $"{cReward}";
        extraText.text = $"{reward}";
        totalText.text = $"{reward + cReward}";
        dayText.text = $"Day {msg.startDate} ~ Day {msg.endDate}";

        petImg.sprite = petAtlas.GetSprite(p.kinds);
        
        Data.Instance.inventory.AddMoney(reward + cReward);
    }
}
