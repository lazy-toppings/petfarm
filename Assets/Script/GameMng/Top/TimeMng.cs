﻿using System;
using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class TimeMng : MonoBehaviour
{
    public MainMng mainMng;
    public MailMng mailMng;
    public RewardMng rewardMng;

    public Transform petT;

    private List<Pet> pets;

    private Text dayText;
    private Text timeText;

    private Coroutine timeRoutine;

    private int[] time;

    private bool isPause;
    private int pauseStack = 0;

    public virtual void Start()
    {
        pets = Data.Instance.petData.pets;
        time = Data.Instance.playData.time;
        dayText = transform.Find("DayText").GetComponent<Text>();
        timeText = transform.Find("TimeText").GetComponent<Text>();

        SetText();
        timeRoutine = StartCoroutine(TimeGoRoutine());
        StartCoroutine(UserPlayTimeRoutine());
    }

    IEnumerator TimeGoRoutine()
    {
        while (true)
        {   
            yield return new WaitForSeconds(1);
            if (isPause) continue;

            SetText();

            if (time[1] == 0)
            {
                // pet 관리
                var removed = new List<Pet>();
                foreach (var pet in pets)
                {
                    if (--pet.dday > 0) continue;

                    Debug.Log("remove " + pet.id);
                    Data.Instance.groundData.RemovePet(pet.id);
                    removed.Add(pet);
                }

                foreach (var pet in removed)
                {
                    Data.Instance.messageData.GetMessage(pet.id).Done();
                    RankEnum rank = pet.Reward(Data.Instance.inventory, Data.Instance.playData);
                    pets.Remove(pet);

                    rewardMng.AddReward(pet, rank);
                }

                if (removed.Count != 0) rewardMng.ShowReward();
                mainMng.InitPets();
                

                // message 관리
                if (Data.Instance.messageData.NextDate <= time[0])
                {
                    mailMng.NewPet(time[0] + 1);
                }
                foreach (Message m in Data.Instance.messageData.IsNew())
                {
                    if (m.startDate <= Data.Instance.playData.time[0])
                    {
                        m.Done();
                    }
                    mailMng.MailGone();
                }

                if (Data.Instance.messageData.IsNew().Count <= 0)
                {
                    mailMng.newMark.SetActive(false);
                }
            }

            time[1]++;
            if (time[1] == 48)
            {
                time[0]++;
                time[1] = 0;
            }

        }
    }

    IEnumerator UserPlayTimeRoutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(60);
            Data.Instance.logData.PlayTime();
        }
    }

    public virtual void PauseTime()
    {
        pauseStack++;
        isPause = true;
        foreach (PetStatusMng st in petT.GetComponentsInChildren<PetStatusMng>(false))
        {
            st.Pause();
        }
    }

    public virtual void PlayTime()
    {
        pauseStack = Math.Max(pauseStack - 1, 0);
        if (pauseStack > 0)
        {
            return;
        }
        isPause = false;
       // Debug.Log("Replay");
        pets = Data.Instance.petData.pets;
        time = Data.Instance.playData.time;
        foreach (PetStatusMng st in petT.GetComponentsInChildren<PetStatusMng>(false))
        {
            st.Play();
        }
    }

    private void SetText()
    {
        dayText.text = "Day " + (time[0] + 1);
        string timeStr = (time[1] / 2) + ":";
        if (time[1] % 2 == 0) timeStr += "00";
        else timeStr += "30";
        timeText.text = timeStr;
    }

    private void SaveData()
    {
        Data.Instance.Save();
        SettingData.Instance.Save();
    }

    private void OnApplicationQuit()
    {
        SaveData();
    }
}