﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;

public class ShowLevelMng : MonoBehaviour
{
    public Text levelText;
    public Text expText;
    public Slider levelInfo;
    public GameObject levelUp;

    public SpriteAtlas petAtlas;

    private float prevValue = -1f;
    private float curValue;
    private bool moving = false;
    private float timer = 0;

    private void Start()
    {
        ShowLevel();
    }

    private void Update()
    {
        if (!moving) return;
        SliderMoving();
    }

    public void ShowLevel()
    {
        levelText.text = $"Lv.{Data.Instance.playData.level+1}";
        UpdateExp();
    }

    public void UpdateExp(bool needSound = true)
    {
        PlayData playData = Data.Instance.playData;
        var threshold = FarmConfig.Instance.LevelConfig.exp;
        var maxLevel = threshold.Count - 1;
        var level = playData.level;
        levelText.text = $"Lv.{playData.level + 1}";

        if (level == maxLevel)
        {
            levelInfo.value = 1000;
            expText.text = $"{playData.exp}/{playData.exp}";
        }
        else if (prevValue == -1)
        {
            var exp = playData.exp;
            prevValue = (float)(exp - threshold[level]) / (threshold[level + 1] - threshold[level]);
            levelInfo.value = prevValue * 1000;
            expText.text = $"{playData.exp}/{threshold[level + 1]}";
        }
        else
        {
            var exp = playData.exp;
            curValue = (float)(exp - threshold[level]) / (threshold[level + 1] - threshold[level]);

            expText.text = $"{playData.exp}/{threshold[level + 1]}";
            if (needSound) GetComponent<AudioSource>().Play();
            moving = true;
        }
    }

    private void SliderMoving()
    {
        if (timer > 1)
        {
            moving = false;
            prevValue = curValue;
            timer = 0;
            return;
        }

        timer += Time.deltaTime * 2f;
        levelInfo.value = Mathf.Lerp(prevValue*1000, curValue*1000, timer);
    }

    public void ShowLevelUp()
    {
        levelUp.SetActive(true);
        levelUp.GetComponent<AudioSource>().Play();

        int level = Data.Instance.playData.level;
        List<string> available = new List<string>();
        foreach (var pet in FarmConfig.Instance.Pets())
        {
            var config = FarmConfig.Instance.PetConfig(pet);
            if (config.level == level + 1) available.Add(pet);
        }

        Transform levelInfo = levelUp.transform.Find("LevelInfo");
        levelInfo.GetChild(0).GetComponent<Image>().sprite = petAtlas.GetSprite(available[0]);

        levelUp.transform.Find("LevelText").GetComponent<Text>().text = $"Level {level+1}";
    }
}
