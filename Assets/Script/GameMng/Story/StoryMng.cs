﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using UnityEngine.U2D;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using UnityEngine.SceneManagement;

public class StoryMng : MonoBehaviour, IPointerClickHandler
{
    public List<AudioSource> sounds;
    public SpriteAtlas images;
    public int storyIdx;
    public string loadedScene;

    public Text topText;
    public Text bottomText;
    public Image img;

    private List<AStory> story;
    private int go = 0;

    private void Start()
    {
        story = Story.Instance.GetStory(storyIdx);
        ShowStory(0);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        go++;
        if (go < story.Count) ShowStory(go);
        else SceneManager.LoadScene(loadedScene);
    }

    private void ShowStory(int go)
    {
        AStory now = story[go];
        
        if (now.sound != "") sounds[Convert.ToInt32(now.sound)].Play();
        
        if (now.image != "") img.sprite = images.GetSprite(now.image);
        topText.text = now.topText;
        bottomText.text = now.bottomText;
    }
}