﻿using UnityEngine;
using UnityEditor;
using UnityEngine.U2D;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using System;

public class ShopMng : MonoBehaviour
{
    public AudioSource failAudio;
    public AudioSource buyAudio;

    public SpriteAtlas itemAtlas;
    public SpriteAtlas petAtlas;
    public ItemBoard itemBoard;
    public TimeMng timeMng;

    public GameObject popup;
    public RectTransform itemContent;
    public RectTransform petContent;
    public RectTransform skinContent;
    public RectTransform actContent;

    public Dictionary<string, List<PetTouchMng>> touchMng = new Dictionary<string, List<PetTouchMng>>();

    private Text titleText;

    private float nextY = 0;
    private int userLevel;

    private void Awake()
    {
        Transform btns = popup.transform.Find("Popup").Find("Btns");
        titleText = popup.transform.Find("TitleText").GetComponent<Text>();
    }

    public void OpenShop()
    {
        LoadItemList();
        timeMng.PauseTime();
    }

    public void Exit()
    {
        timeMng.PlayTime();
        Close();
    }

    public void Close()
    {
        popup.SetActive(false);
        ClosePetAct();
    }

    private void LoadItemList(List<string> items)
    {
        ClosePetAct();
        
        GetComponent<AudioSource>().Play();
        titleText.text = "Items";
        Vector2 contentPos = itemContent.localPosition;
        itemContent.localPosition = contentPos;
        popup.SetActive(true);
        nextY = 0;

        itemContent.parent.parent.gameObject.SetActive(true);
        petContent.parent.parent.gameObject.SetActive(false);
        if (itemContent.childCount != 0)
        {
            foreach (Transform child in itemContent)
            {
                child.Find("NotEnough").GetComponent<Text>().text = "";
            }
            return;
        }

        foreach (string i in items)
        {
            GameObject g = Instantiate(Resources.Load("Prefabs/UI/Shop/ShopItemList")) as GameObject;

            CloneList(itemContent, g);

            Item item = new Item(i);
            g.transform.Find("ItemImg").GetComponent<Image>().sprite = itemAtlas.GetSprite(i);
            g.transform.Find("PriceText").GetComponent<Text>().text = (item.Price * 3).ToString();
            g.transform.Find("InfoText").GetComponent<Text>().text = "x 3";

            g.GetComponentInChildren<Button>().onClick.AddListener(() =>
            {
                BuyItem(item, g.transform.Find("NotEnough").GetComponent<Text>());
            });
        }

        GameObject allG = Instantiate(Resources.Load("Prefabs/UI/Shop/ShopItemAllList")) as GameObject;
        CloneList(itemContent, allG);
        int price = FarmConfig.Instance.AllItemPrice();
        allG.transform.Find("PriceText").GetComponent<Text>().text = $"{price * 3}";
        allG.transform.Find("InfoText").GetComponent<Text>().text = "x 3";

        allG.GetComponentInChildren<Button>().onClick.AddListener(() =>
        {
            BuyAllItem(price, allG.transform.Find("NotEnough").GetComponent<Text>());
        });
    }

    public void LoadPetList()
    {
        GetComponent<AudioSource>().Play();
        titleText.text = "Pet book";
        Vector2 contentPos = petContent.localPosition;
        petContent.localPosition = contentPos;
        popup.SetActive(true);
        nextY = 0;
        
        itemContent.parent.parent.gameObject.SetActive(false);
        petContent.parent.parent.gameObject.SetActive(true);
        if (petContent.childCount != 0)
        {
            for (int j = 0; j < petContent.childCount; ++j)
            {
                userLevel = Data.Instance.playData.level + 1;
                Transform t = petContent.GetChild(j);
                if (t.name == userLevel.ToString()) t.Find("Inactive").gameObject.SetActive(false);
            }
            return;
        }

        userLevel = Data.Instance.playData.level + 1;
        int i = 0;
        foreach (string p in FarmConfig.Instance.Pets())
        {
            int xDelta = 0;
            int yDelta = 0;
            GameObject g;

            int level = FarmConfig.Instance.PetConfig(p).level;
            if (FarmConfig.Instance.PetConfig(p).chain != "petfarm")
            {
                g = Instantiate(Resources.Load("Prefabs/UI/MyPet/NotMyPetList")) as GameObject;
                g.GetComponentInChildren<Text>().text = $"You can buy a pet in {FarmConfig.Instance.PetConfig(p).chain}";
            }
            else
            {
                g = Instantiate(Resources.Load("Prefabs/UI/Shop/ShopPet")) as GameObject;
                Transform actBtn = g.transform.Find("ActBtn"); 
                actBtn.GetComponent<Button>().onClick.AddListener(() => LoadAct(p));
                actBtn.Find("PetNameText").GetComponent<Text>().text = FarmConfig.Instance.PetConfig(p).name;

                if (level > userLevel)
                {
                    g.transform.Find("Inactive").gameObject.SetActive(true);
                    g.transform.Find("Inactive").GetComponentInChildren<Text>().text = $"Open at Lv.{level}"; 
                }
                g.name = level.ToString();
            }
            g.transform.Find("PetImg").GetComponent<Image>().sprite = petAtlas.GetSprite(p);

            if (i % 2 == 1)
            {
                yDelta = -(int)g.GetComponent<RectTransform>().sizeDelta.y;
                xDelta = (int)g.GetComponent<RectTransform>().sizeDelta.x;
            }

            i++;
            g.transform.SetParent(petContent);
            g.transform.localPosition = new Vector2(xDelta, nextY);
            g.transform.localScale = new Vector2(1, 1);
            nextY += yDelta;
            if (-nextY > petContent.sizeDelta.y) petContent.sizeDelta = new Vector2(petContent.sizeDelta.x, -nextY);
        }
    }

    private void LoadAct(string p)
    {
        GetComponent<AudioSource>().Play();
        actContent.parent.parent.gameObject.SetActive(true);

        if (actContent.Find(p) != null)
        {
            actContent.Find(p).gameObject.SetActive(true);
            return;
        }
        GameObject parent = new GameObject();
        parent.name = p;
        parent.AddComponent<RectTransform>();
        parent.GetComponent<RectTransform>().anchorMin = new Vector2(0, 1);
        parent.GetComponent<RectTransform>().anchorMax = new Vector2(0, 1);
        parent.transform.SetParent(actContent);
        parent.transform.localPosition = new Vector2(0, 0);

        int i = 0;
        nextY = 0;
        foreach (APetItem item in PetItem.Instance.act[p])
        {
            int xDelta = 0;
            int yDelta = 0;
            GameObject g;
            MyPetItem pItem = Data.Instance.inventory.HasAct(p, item.param, item.id);
            if (pItem != null)
            {
                g = SetBoughtPetItem(p, item, pItem);
            }
            else
            {
                g = Instantiate(Resources.Load("Prefabs/UI/Shop/BuyPetItem")) as GameObject;
                g.transform.SetParent(parent.transform);
                Text moneyText = g.transform.Find("MoneyText").GetComponent<Text>();
                moneyText.text = item.price.ToString();
                SetItemPetImg(p, item.id, g.transform.Find("PetImg"));
                g.transform.Find("InfoText").GetComponent<Text>().text = item.info;
                g.transform.Find("BuyBtn").GetComponent<Button>().onClick.AddListener(() =>
                    BuyAct(p, item, item.price, moneyText, g.transform.GetSiblingIndex()));
            }
            
            if (i % 2 == 1)
            {
                yDelta = -(int)g.GetComponent<RectTransform>().sizeDelta.y;
                xDelta = (int)g.GetComponent<RectTransform>().sizeDelta.x;
            }

            i++;
            g.transform.localPosition = new Vector2(xDelta, nextY);
            g.transform.localScale = new Vector2(1, 1);
            nextY += yDelta;
            if (-nextY > actContent.sizeDelta.y) actContent.sizeDelta = new Vector2(actContent.sizeDelta.x, -nextY);
        }
    }

    public void ClosePetAct()
    {
        foreach (Transform t in actContent) t.gameObject.SetActive(false);
        actContent.parent.parent.gameObject.SetActive(false);
    }

    private void SetItemPetImg(string p, int id, Transform parent)
    {
        GameObject img = Instantiate(Resources.Load($"Prefabs/PetItem/{p}Act{id}")) as GameObject;
        img.transform.SetParent(parent);
        img.transform.localPosition = new Vector2(0, 0);
    }

    private GameObject SetBoughtPetItem(string p, APetItem item, MyPetItem pItem)
    {
        GameObject g = Instantiate(Resources.Load("Prefabs/UI/Shop/BoughtPetItem")) as GameObject;
        g.transform.SetParent(actContent.Find(p));
        Transform useBtn = g.transform.Find("UseBtn");
        useBtn.gameObject.SetActive(!pItem.inUse);
        if (!pItem.inUse) useBtn.GetComponent<Button>().onClick.AddListener(() => UseAct(p, item, g.transform.GetSiblingIndex()));
        g.transform.Find("InUse").gameObject.SetActive(pItem.inUse);
        SetItemPetImg(p, item.id, g.transform.Find("PetImg"));
        g.transform.Find("InfoText").GetComponent<Text>().text = item.info;
        return g;
    }

    private void BuyAct(string p, APetItem item, int price, Text notEnough, int idx)
    {
        if (Data.Instance.inventory.money < price)
        {
            failAudio.Play();
            notEnough.text = "Not enough money";
            StartCoroutine(WaitSeconds(2, () => notEnough.text = $"{price}"));
            return;
        }
        buyAudio.Play();
        Data.Instance.inventory.AddMoney(-price);
        Data.Instance.inventory.AddPetAct(p, item.param, item.id);
        ChangeToBuyAct(p, item, Data.Instance.inventory.HasAct(p, item.param, item.id), idx);
        
        if (touchMng.ContainsKey(p)) foreach (PetTouchMng m in touchMng[p]) if (m.gameObject.activeSelf) m.SetActs();
    }

    private void UseAct(string p, APetItem item, int idx)
    {
        Data.Instance.inventory.UseAct(p, item.param, item.id);
        actContent.Find(p).GetChild(idx).Find("UseBtn").gameObject.SetActive(false);
        actContent.Find(p).GetChild(idx).Find("InUse").gameObject.SetActive(true);
    }

    private void ChangeToBuyAct(string p, APetItem item, MyPetItem pItem, int idx)
    {
        Transform prev = actContent.Find(p).GetChild(idx);
        prev.gameObject.SetActive(false);
        GameObject g = SetBoughtPetItem(p, item, pItem);
        g.transform.localPosition = prev.localPosition;
    }

    public void LoadItemList()
    {
        LoadItemList(FarmConfig.Instance.Items());
    }

    private void CloneList(RectTransform content, GameObject g)
    {
        g.transform.SetParent(content);
        g.transform.localPosition = new Vector2(0, nextY);
        g.transform.localScale = new Vector2(1, 1);
        nextY -= (g.GetComponent<RectTransform>().sizeDelta.y + 35);

        if (-nextY > content.sizeDelta.y) content.sizeDelta = new Vector2(content.sizeDelta.x, -nextY);
    }

    private void BuyAllItem(int price, Text notEnough)
    {
        Inventory inventory = Data.Instance.inventory;
        if (inventory.money < price * 3)
        {
            failAudio.Play();
            notEnough.text = "Not enough coin";
            StartCoroutine(WaitSeconds(2f, () => notEnough.text = ""));
            return;
        }
        buyAudio.Play();
        inventory.AddAllItem(3);
        inventory.AddMoney(-price * 3);
        itemBoard.LoadCurPage();
    }

    private void BuyItem(Item item, Text notEnough)
    {
        Inventory inventory = Data.Instance.inventory;
        if (inventory.money < item.Price * 3)
        {
            failAudio.Play();
            notEnough.text = "Not enough coin";
            StartCoroutine(WaitSeconds(2f, () => notEnough.text = ""));
            return;
        }
        buyAudio.Play();
        inventory.AddItem(item.Kinds, 3);
        inventory.AddMoney(-item.Price * 3);
        Data.Instance.logData.BuyTheItem(item.Kinds);
        itemBoard.LoadCurPage();
    }

    private IEnumerator WaitSeconds(float seconds, Action action)
    {
        yield return new WaitForSeconds(seconds);
        action();
    }
}