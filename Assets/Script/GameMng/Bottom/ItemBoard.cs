﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.U2D;

public class ItemBoard : MonoBehaviour
{
    public SpriteAtlas itemAtlas;

    public Camera cam;
    public Transform building;
    public Transform pets;

    public float gradient;
    public float yInter;
    private float xLen;
    public float yLen;

    private int curPage = 0;

    private void Start()
    {
        float[] xPos = new float[2];
        float[] yPos = new float[2];
        xPos[0] = cam.WorldToScreenPoint(building.GetChild(1).GetChild(0).position).x;
        xPos[1] = cam.WorldToScreenPoint(building.GetChild(1).GetChild(1).position).x;

        yPos[0] = cam.WorldToScreenPoint(building.GetChild(1).GetChild(0).position).y;
        yPos[1] = cam.WorldToScreenPoint(building.GetChild(1).GetChild(1).position).y;

        xLen = xPos[1] - xPos[0];
        yLen = yPos[0] - yPos[1];

        gradient = yLen / xLen;
        yInter = yPos[1] - yLen;

        LoadItem(0);
        transform.parent.GetComponentInChildren<Button>().onClick.AddListener(() => LoadItem(curPage == 0 ? 1 : 0));
    }

    public void LoadCurPage()
    {
        LoadItem(curPage);
    }

    private void LoadItem(int page)
    {
        Close();
        int cnt = -1;
        int l = 0;
        curPage = page;
        foreach (string i in Data.Instance.inventory.GetSortedItem())
        {
            cnt++;
            if (cnt < page * 5 || cnt >= (page + 1) * 5) continue;

            GameObject go = Instantiate(Resources.Load($"Prefabs/UI/Item/ItemList")) as GameObject;
            go.GetComponentInChildren<Image>().sprite = itemAtlas.GetSprite(i);
            go.GetComponentInChildren<Text>().text = $"{Data.Instance.inventory.item[i]}";
            go.GetComponentInChildren<Drag>().itemBoard = GetComponent<ItemBoard>();
            go.GetComponentInChildren<Drag>().pets = pets;
            go.GetComponentInChildren<Drag>().itemKind = i;
            go.transform.SetParent(transform);
            go.transform.localScale = new Vector3(1, 1, 1);
            go.transform.localPosition = new Vector2(go.GetComponent<RectTransform>().sizeDelta.x * l++, 0);
        }

    }

    private void Close()
    {
        for (int i = 0; i < transform.childCount; ++i) transform.GetChild(i).gameObject.SetActive(false);
    }
}
