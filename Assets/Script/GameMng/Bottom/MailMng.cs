﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;
using Random = System.Random;

public class MailMng : MonoBehaviour
{
    public SpriteAtlas petAtlas;
    public SpriteAtlas fenceAtlas;

    public PetBuildMng petBuildMng;
    public FenceBuildMng fenceBuildMng;
    public MyPetMng myPetMng;
    public StripeTokenMng stripeTokenMng;
    public TimeMng timeMng;
    
    private static int DESTROY_PRICE = 5000;

    public GameObject popup;
    public RectTransform mailContent;
    public RectTransform fenceContent;
    public Transform buyLand;
    public GameObject newMark;

    public AudioSource failAudio;
    public AudioSource openAudio;
    public AudioSource acceptAudio;
    public AudioSource declineAudio;
    public AudioSource buyAudio;

    private float deltaX, deltaY, nextY = 0;
    private GameObject mailOn;
    private GameObject fenceOn;
    private Text titleText;

    private void Awake()
    {
        deltaX = 270.06f;
        deltaY = 390.9f;

        Transform btns = popup.transform.Find("Btns");
        titleText = popup.transform.Find("TitleText").GetComponent<Text>();
        mailOn = btns.Find("MailOn").gameObject;
        fenceOn = btns.Find("FenceOn").gameObject;
    }

    private void Start()
    {
        if (Data.Instance.messageData.IsNew().Count > 0)
        {
            newMark.SetActive(true);
        }
        else
        {
            newMark.SetActive(false);
        }
    }

    public void NewPet(int startDate)
    {
        newMark.SetActive(true);
        var pet = WeightedRandomPets();
        var data = Data.Instance.messageData;
        var period = new Random().Next(3, Data.Instance.playData.level + 4);
        data.AddMessage(new Message(data.GetId(), startDate, period, null, pet));
    }

    public void MailGone()
    {
        if (Data.Instance.messageData.IsNew() == null)
        {
            newMark.SetActive(false);
        }
    }

    public void OpenMail()
    {
        popup.SetActive(true);

        LoadMail();
        timeMng.PauseTime();
    }

    public void LoadMail()
    {
        titleText.text = "Requests";
        RemoveMail();
        Vector2 contentPos = mailContent.localPosition;
        mailContent.localPosition = contentPos;

        mailOn.SetActive(true);
        fenceOn.SetActive(false);

        mailContent.parent.parent.gameObject.SetActive(true);
        fenceContent.parent.parent.gameObject.SetActive(false);

        openAudio.Play();

        var messages = Data.Instance.messageData.GetActiveMessage();
        messages.Reverse();
        foreach (Message m in messages)
        {
            if (m.status == "Done" || m.status == "Refused") continue;
            int yDelta;
            GameObject g;
            if (m.status == "Waiting")
            {
                g = Instantiate(Resources.Load("Prefabs/UI/Message/MessageList")) as GameObject;

                Button acceptBtn = g.transform.Find("AcceptBtn").GetComponent<Button>();
                acceptBtn.onClick.AddListener(() => AcceptPet(m, acceptBtn.GetComponentInChildren<Text>()));

                Button declineBtn = g.transform.Find("DeclineBtn").GetComponent<Button>();
                declineBtn.onClick.AddListener(() => DeclinePet(m));

                g.transform.Find("RewardText").GetComponent<Text>().text = $"{m.cost} + a";
            }
            else
            {
                g = Instantiate(Resources.Load("Prefabs/UI/Message/MyMessageList")) as GameObject;

                g.transform.Find("RewardText").GetComponent<Text>().text = $"{m.cost} + a";
            }

            yDelta = -(int)g.GetComponent<RectTransform>().sizeDelta.y - 35;

            g.transform.SetParent(mailContent);
            g.transform.localPosition = new Vector2(0, nextY);
            g.transform.localScale = new Vector2(1, 1);
            nextY += yDelta;
            if (-nextY > mailContent.sizeDelta.y) mailContent.sizeDelta = new Vector2(mailContent.sizeDelta.x, -nextY);

            g.transform.Find("PetImg").GetComponent<Image>().sprite = petAtlas.GetSprite(m.petType.ToString());
            g.transform.Find("PeriodText").GetComponent<Text>().text = $"Day {m.startDate+1} ~ Day {m.endDate}";

            List<Buildings> fences = FarmConfig.Instance.PetConfig(m.petType).fences;
            g.transform.Find("Fence1Img").GetComponent<Image>().sprite = fenceAtlas.GetSprite(fences[0].ToString());
            if (fences.Count > 1) g.transform.Find("Fence2Img").GetComponent<Image>().sprite = fenceAtlas.GetSprite(fences[1].ToString());
        }

        if (Data.Instance.id == null)
        {
            GameObject chainG = Instantiate(Resources.Load("Prefabs/UI/Message/ChainMessageList")) as GameObject;
            chainG.transform.Find("ConnectBtn").GetComponent<Button>().onClick.AddListener(() =>
            {
                stripeTokenMng.Open("signup");
                Close();
            });
            chainG.transform.SetParent(mailContent);
            chainG.transform.localPosition = new Vector2(0, nextY);
            chainG.transform.localScale = new Vector2(1, 1);
            nextY -= ((int)chainG.GetComponent<RectTransform>().sizeDelta.y + 35);
            if (-nextY > mailContent.sizeDelta.y) mailContent.sizeDelta = new Vector2(mailContent.sizeDelta.x, -nextY);

        }
    }

    public void LoadFenceList()
    {
        titleText.text = "Land";
        mailOn.SetActive(false);
        fenceOn.SetActive(true);
        popup.SetActive(true);
        nextY = 0;

        mailContent.parent.parent.gameObject.SetActive(false);
        fenceContent.parent.parent.gameObject.SetActive(true);

        fenceContent.parent.parent.Find("CapacityText").GetComponent<Text>().text = $"Capacity: {Data.Instance.inventory.land}, Have: {Data.Instance.groundData.OccupiedCount()}" ;
        if (fenceContent.childCount != 0)
        {
            foreach (Transform child in fenceContent)
            {
                if (child.name == "ShopFenceList(Clone)")
                    child.Find("PriceText").GetComponent<Text>().text = FencePrice.ToString();
                else
                    child.Find("PriceText").GetComponent<Text>().text = DESTROY_PRICE.ToString();
                child.Find("NotEnough").GetComponent<Text>().text = "";
            }
            return;
        }
        foreach (Fence f in FenceFactory.GetFences())
        {
            GameObject g = Instantiate(Resources.Load("Prefabs/UI/Shop/ShopFenceList")) as GameObject;

            CloneList(fenceContent, g);
            
            g.transform.Find("ItemImg").GetComponent<Image>().sprite = fenceAtlas.GetSprite(f.Kinds.ToString());
            g.transform.Find("PriceText").GetComponent<Text>().text = FencePrice.ToString();

            g.GetComponentInChildren<Button>().onClick.AddListener(() =>
            {
                BuyFence(f, g.transform.Find("NotEnough").GetComponent<Text>());
            });
        }

        GameObject go = Instantiate(Resources.Load("Prefabs/UI/Shop/ShopFenceDestroy")) as GameObject;
        CloneList(fenceContent, go);

        go.transform.Find("PriceText").GetComponent<Text>().text = DESTROY_PRICE.ToString();
        go.GetComponentInChildren<Button>().onClick.AddListener(() =>
        {
            DestroyFence(go.transform.Find("NotEnough").GetComponent<Text>());
        });
    }

    public void BuyFence(Fence f, Text notEnough)
    {
        Inventory inventory = Data.Instance.inventory;
        var groundData = Data.Instance.groundData;
        int[] xy = Data.Instance.groundData.GetEmptySpace();
        if (xy[0] == -1)
        {
            failAudio.Play();
            notEnough.text = "There's no empty space";
            StartCoroutine(WaitSeconds(2f, () => notEnough.text = ""));

            return;
        }
        if (!groundData.ExistExtra())
        {
            failAudio.Play(); OpenBuyLandPopup();
            StartCoroutine(WaitSeconds(2f, () => notEnough.text = ""));
            return;
        }
        if (inventory.money < FencePrice)
        {
            failAudio.Play();
            notEnough.text = "Not enough coin";
            StartCoroutine(WaitSeconds(2f, () => notEnough.text = ""));
            return;
        }
        buyAudio.Play();

        Exit();
        fenceBuildMng.BuildBuilding(f.Kinds, true, FencePrice);
    }

    private void OpenBuyLandPopup()
    {
        buyLand.gameObject.SetActive(true);
        int capa = Data.Instance.inventory.land;
        buyLand.Find("InfoText").GetComponent<Text>().text = $"Your capacity of land is {capa}.\nTo make capacity {capa + 1},";
        buyLand.Find("PayMoneyBtn").Find("CostText").GetComponent<Text>().text =
            $"{FarmConfig.Instance.landExpand[capa]}";
    }

    public void BuyANewPet()
    {
        buyLand.gameObject.SetActive(false);
        Exit();
        myPetMng.OpenBox();
    }

    public void BuyLand()
    {
        int capa = Data.Instance.inventory.land;

        Text moneyText = buyLand.Find("PayMoneyBtn").Find("CostText").GetComponent<Text>();
        if (Data.Instance.inventory.money < FarmConfig.Instance.landExpand[capa])
        {
            moneyText.text = "Not enough coin";
            StartCoroutine(WaitSeconds(1.5f,() => moneyText.text = $"{FarmConfig.Instance.landExpand[capa]}"));
            return;
        }
        Data.Instance.inventory.AddMoney(-FarmConfig.Instance.landExpand[capa]);
        Data.Instance.inventory.AddLand();
        OpenBuyLandPopup();
        buyLand.gameObject.SetActive(false);
        fenceContent.parent.parent.Find("CapacityText").GetComponent<Text>().text = $"Capacity: {Data.Instance.inventory.land}, Have: {Data.Instance.groundData.OccupiedCount()}";
    }

    private void DestroyFence(Text notEnough)
    {
        Inventory inventory = Data.Instance.inventory;
        int[] xy = Data.Instance.groundData.GetEmptyBuilding();
        if (xy[0] == -1)
        {
            failAudio.Play();
            notEnough.text = "There are no empty land to destroy";
            StartCoroutine(WaitSeconds(2f, () =>
            {
                notEnough.text = "";
            }));
            return;
        }
        if (inventory.money < DESTROY_PRICE)
        {
            failAudio.Play();
            notEnough.text = "Not enough coin";
            StartCoroutine(WaitSeconds(2f, () => notEnough.text = ""));
            return;
        }
        buyAudio.Play();

        Exit();
        fenceBuildMng.DestroyBuilding(true, DESTROY_PRICE);
    }


    private void CloneList(RectTransform content, GameObject g)
    {
        g.transform.SetParent(content);
        g.transform.localPosition = new Vector2(0, nextY);
        g.transform.localScale = new Vector2(1, 1);
        nextY -= (g.GetComponent<RectTransform>().sizeDelta.y + 35);

        if (-nextY > content.sizeDelta.y) content.sizeDelta = new Vector2(content.sizeDelta.x, -nextY);
    }

    public void AcceptPet(Message m, Text text)
    {
        var pet = Data.Instance.petData.GetNewPet(m.petType, m.id, m.endDate - m.startDate + 1);
        int[] empty = Data.Instance.groundData.GetEmptyBuilding(pet.Fences);
        if (empty[0] == -1)
        {
            failAudio.Play();
            text.text = "check needs";
            StartCoroutine(WaitSeconds(2f, () => text.text = "Accept"));
            return;
        }

        acceptAudio.Play();
        newMark.SetActive(false);
        petBuildMng.BuildPet(pet, true, m.startDate);
        Exit();
    }

    public void DeclinePet(Message m)
    {
        declineAudio.Play();
        newMark.SetActive(false);
        Data.Instance.messageData.ChangeStatus(m.id, "Refused");
        Data.Instance.logData.DeclinePet();
        LoadMail();
    }

    public void Exit()
    {
        Close();
    }

    private void RemoveMail()
    {
        for (int i = 0; i < mailContent.childCount; ++i)
        {
            mailContent.GetChild(i).gameObject.SetActive(false);
        }
        nextY = 0;
    }

    private void Close()
    {
        timeMng.PlayTime();
        popup.SetActive(false);
        mailContent.sizeDelta = new Vector2(deltaX, deltaY);
        mailContent.localPosition = new Vector2(0, 0);
        StopAllCoroutines();
    }
    
    private List<string> AvailablePets()
    {
        var ret = new List<string>();
        var filter = new List<string>();
        var level = Data.Instance.playData.level + 1;
        foreach (var pet in FarmConfig.Instance.Pets())
        {
            var config = FarmConfig.Instance.PetConfig(pet);
            if (config.level <= level && config.chain == "petfarm") { ret.Add(pet); filter.Add(pet); }
        }

        foreach (string pet in Data.Instance.myPet.pets.Keys)
        {
            if (!ret.Contains(pet)) { ret.Add(pet); filter.Add(pet); }
        }
        FilterExistingPets(filter);
        if (filter.Count == 0) return ret;
        else return filter;
    }

    private void FilterExistingPets(List<string> typeList)
    {
        var typeSet = new HashSet<string>();
        foreach (var pet in Data.Instance.petData.pets)
        {
            typeSet.Add(pet.kinds);
        }
        
        if (typeSet.Count == typeList.Count) return;
        foreach (var type in typeSet)
        {
            typeList.Remove(type);
        }
    }

    private string WeightedRandomPets()
    {
        if (!Data.Instance.playData.tutoDone) return "Pet1";
        var pets = AvailablePets();
        return pets[UnityEngine.Random.Range(0, pets.Count)];
    }

    private IEnumerator WaitSeconds(float seconds, Action action)
    {
        yield return new WaitForSeconds(seconds);
        action();
    }

    private int FencePrice => Data.Instance.groundData.Price();
}
