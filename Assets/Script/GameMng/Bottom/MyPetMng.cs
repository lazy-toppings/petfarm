﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.U2D;

public class MyPetMng : PopupMng
{
    public SpriteAtlas petAtlas;
    public SpriteAtlas uiAtlas;
    public NotSignedInPopupMng notSignedInPopupMng;
    public StripeTokenMng stripeTokenMng;

    public GameObject petOn;
    public GameObject boxOn;
    public GameObject box;
    public TimeMng timeMng;

    public AudioSource upgradeSound;
    public AudioSource failSound;

    public Post post;
    private PetWorkMng petWorkMng;

    private Text titleText;
    private Transform petInfo;
    private Image petImg;
    private Text countText;
    private Text levelText;
    private Text effectText;
    private Slider slider;
    private Slider levelUpSlider;
    private Text levelUpCostText;
    private Button levelUpBtn;
    private Button workBtn;
    private Text workText;
    private GameObject coinImg;

    private bool reload;


    private void Awake()
    {
        deltaX = 804.4f;
        deltaY = 112;
    }

    private void Start()
    {
        petWorkMng = GetComponent<PetWorkMng>();

        titleText = popup.transform.Find("TitleText").GetComponent<Text>();
        petInfo = popup.transform.Find("PetInfo");
        petImg = petInfo.Find("PetImg").GetComponent<Image>();
        countText = petInfo.Find("CountText").GetComponent<Text>();
        levelText = petInfo.Find("LevelText").GetComponent<Text>();
        effectText = petInfo.Find("EffectText").GetComponent<Text>();
        levelUpSlider = petInfo.Find("LevelUpSlider").GetComponent<Slider>();
        slider = petInfo.Find("Slider").GetComponent<Slider>();
        levelUpBtn = petInfo.Find("LevelUpBtn").GetComponent<Button>();
        levelUpCostText = levelUpBtn.transform.Find("LevelUpCostText").GetComponent<Text>();
        workBtn = petInfo.Find("WorkBtn").GetComponent<Button>();
        workText = workBtn.transform.Find("WorkText").GetComponent<Text>();
        coinImg = workBtn.transform.Find("CoinImg").gameObject;
    }

    public void LoadBox()
    {
        StopAllCoroutines();
        GetComponent<AudioSource>().Play();
        titleText.text = "Pet Egg Shop";
        petInfo.gameObject.SetActive(false);
        content.gameObject.SetActive(false);
        boxOn.SetActive(true);
        box.SetActive(true);
    }

    public void OpenBox()
    {
        popup.SetActive(true);
        timeMng.PauseTime();
        LoadBox();
    }

    public void OpenPet()
    {
        timeMng.PauseTime();
        LoadPet();
    }

    public void LoadPet()
    {
        Close(content.childCount == 0 || reload);
        content.gameObject.SetActive(true);
        petOn.SetActive(true);
        GetComponent<AudioSource>().Play();
        popup.SetActive(true);
        titleText.text = "My Pets";
        if (!(content.childCount == 0) && !reload) return;

        reload = false;
        int i = 0;
        if (Data.Instance.id == null)
        {
            GameObject chainObj = Instantiate(Resources.Load("Prefabs/UI/MyPet/ChainConnectList")) as GameObject;
            chainObj.transform.Find("ConnectBtn").GetComponent<Button>().onClick.AddListener(() => ConnectChain());
            chainObj.transform.SetParent(content);
            chainObj.transform.localPosition = new Vector2(44, nextY);
            nextY -= chainObj.GetComponent<RectTransform>().sizeDelta.y;
        }
        foreach (string p in Data.Instance.myPet.PetInSortedOrder())
        {
            int xDelta = 0;
            int yDelta = 0;
            GameObject g;

            if (!Data.Instance.myPet.IsMine(p))
            {
                g = Instantiate(Resources.Load("Prefabs/UI/MyPet/NotMyPetList")) as GameObject;
                g.GetComponentInChildren<Button>().onClick.AddListener(() => LoadBox());

                if (FarmConfig.Instance.PetConfig(p).chain != "petfarm")
                {
                    g.transform.Find("LevelText").GetComponent<Text>().text = $"You can buy a pet in {FarmConfig.Instance.PetConfig(p).chain}";
                    g.transform.Find("ToShopBtn").GetComponent<Button>().interactable = false;
                }
            }
            else
            {
                g = SetAPet(p);
            }
            g.transform.Find("PetImg").GetComponent<Image>().sprite = petAtlas.GetSprite(p);

            if (i % 2 == 1)
            {
                yDelta = -(int)g.GetComponent<RectTransform>().sizeDelta.y;
                xDelta = (int)g.GetComponent<RectTransform>().sizeDelta.x;
            }

            i++;
            g.transform.SetParent(content);
            g.transform.localPosition = new Vector2(xDelta, nextY);
            g.transform.localScale = new Vector2(1, 1);
            nextY += yDelta;
            if (-nextY > content.sizeDelta.y) content.sizeDelta = new Vector2(content.sizeDelta.x, -nextY);

        }
    }

    private GameObject SetAPet(string p)
    {
        int count = Data.Instance.myPet.pets[p].count;
        int level = Data.Instance.myPet.pets[p].level;
        int petTier = PetEffect.Instance.petTier[level + 1];

        GameObject g;
        if (count >= petTier) g = Instantiate(Resources.Load("Prefabs/UI/MyPet/MyPetLevelUpList")) as GameObject;
        else g = Instantiate(Resources.Load("Prefabs/UI/MyPet/MyPetList")) as GameObject;
        g.transform.Find("LevelText").GetComponent<Text>().text = $"Lv. {level + 1}";
        g.transform.Find("CountText").GetComponent<Text>().text = $"{count} / {petTier}";
        g.GetComponentInChildren<Slider>().value = 1f * count / petTier;
        g.GetComponentInChildren<Button>().onClick.AddListener(() => OpenPetInfo(p, g.transform.GetSiblingIndex()));
        if (Data.Instance.myPet.works[p].isReward) g.transform.Find("NewMark").gameObject.SetActive(true);
        return g;
    }

    public void ClosePetInfo()
    {
        petInfo.gameObject.SetActive(false);
        StopAllCoroutines();
    }

    private void OpenPetInfo(string p, int idx)
    {
        StopAllCoroutines();
        GetComponent<AudioSource>().Play();
        int count = Data.Instance.myPet.pets[p].count;
        int level = Data.Instance.myPet.pets[p].level;
        int petTier = PetEffect.Instance.petTier[level + 1];

        petInfo.gameObject.SetActive(true);
        petImg.sprite = petAtlas.GetSprite(p);
        countText.text = $"{count} / {petTier}";
        levelText.text = $"Lv. {level + 1}";
        
        if (count >= petTier)
        {
            slider.gameObject.SetActive(false);
            levelUpSlider.gameObject.SetActive(true);
            levelUpSlider.value = 1f * count / petTier;
            if (level != PetEffect.Instance.petTier.Count - 2) levelUpBtn.interactable = true;
            else levelUpBtn.interactable = false;
        }
        else
        {
            levelUpSlider.gameObject.SetActive(false);
            slider.gameObject.SetActive(true);
            slider.value = 1f * count / petTier;
            levelUpBtn.interactable = false;
        }

        PetWork work = PetEffect.Instance.work.Find(w => w.type == p);
        int earn = work.earn[level];
        int time = work.time[level];
        int cost = work.cost[level + 1];
        effectText.text = $"Earns {earn} coin per 1 work\nTakes {time} minutes for 1 Work";
        levelUpCostText.text = cost.ToString();
        levelUpBtn.onClick.RemoveAllListeners();
        levelUpBtn.onClick.AddListener(() => LevelUp(p, cost, idx));

        MyPetWork petWork = Data.Instance.myPet.works[p];
        if (petWork.isWork)
        {
            SetWorkStatus(p, time, petWork.workTime, idx);
        }
        else if (petWork.isReward)
        {
            SetRewardStatus(p, earn, idx);
        }
        else
        {
            SetNothingStatus(p, idx);
        }
    }
    

    private void SetRewardStatus(string p, int earn, int idx)
    {
        workText.text = $"     {earn}";
        workBtn.onClick.RemoveAllListeners();
        workBtn.interactable = true;
        coinImg.SetActive(true);
        workBtn.onClick.AddListener(() => GetReward(p, earn, idx));
    }

    private void SetWorkStatus(string p, int time, DateTime workTime, int idx)
    {
        coinImg.SetActive(false);
        workBtn.interactable = false;
        StartCoroutine(ShowRemainTimeRoutine(p, TimeSpan.FromMinutes(time).Subtract(DateTime.Now.Subtract(workTime)), idx));
    }

    private void SetNothingStatus(string p, int idx)
    {
        workText.text = "Send work";
        coinImg.SetActive(false);
        workBtn.interactable = true;
        workBtn.onClick.RemoveAllListeners();
        workBtn.onClick.AddListener(() => SendWork(p, idx));
    }

    private void GetReward(string p, int earn, int idx)
    {
        content.GetChild(idx).Find("NewMark").gameObject.SetActive(false);
        Data.Instance.inventory.AddMoney(earn);
        Data.Instance.myPet.GetReward(p);
        SetNothingStatus(p, idx);
        petWorkMng.CheckNew();
    }

    private void SendWork(string p, int idx)
    {
        DateTime workTime = Data.Instance.myPet.SendWork(p);
        SetWorkStatus(p, PetEffect.Instance.work.Find(w => w.type == p).time[Data.Instance.myPet.pets[p].level], workTime, idx);
        petWorkMng.CheckDone(p, workTime, content.GetChild(idx).Find("NewMark").gameObject);
    }

    private void LevelUp(string p, int cost, int idx)
    {
        int level = Data.Instance.myPet.pets[p].level;
        int count = Data.Instance.myPet.pets[p].count;
        PetWork work = PetEffect.Instance.work.Find(w => w.type == p);
        int petTier = PetEffect.Instance.petTier[level + 1];

        upgradeSound.Play();
        if (Data.Instance.inventory.money < cost)
        {
            StartCoroutine(NoMoneyRoutine(cost));
            return;
        }
        Data.Instance.inventory.AddMoney(-cost);
        Data.Instance.myPet.LevelUp(p);
        OpenPetInfo(p, idx);
        GameObject g = SetAPet(p);
        g.transform.Find("PetImg").GetComponent<Image>().sprite = petAtlas.GetSprite(p);
        g.transform.SetParent(content);
        g.transform.localPosition = content.GetChild(idx).localPosition;
        content.GetChild(idx).gameObject.SetActive(false);
    }

    private void ConnectChain()
    {
        Exit();
        notSignedInPopupMng.Open();
    }

    public void BuyBasicBox()
    {
        if (Data.Instance.id == null)
        {
            ConnectChain();
        }
        else
        {
            stripeTokenMng.Open("Basic");
            reload = true;
        }
    }

    public void BuyStandardBox()
    {
        if (Data.Instance.id == null)
        {
            ConnectChain();
        }
        else
        {
            stripeTokenMng.Open("Standard");
            reload = true;
        }
    }

    public void ShowGetPet(string p)
    {
        upgradeSound.Play();
        bool isNew = Data.Instance.myPet.pets[p].count == 1;
        Transform boxT = box.transform.Find("GetPet");
        boxT.gameObject.SetActive(true);
        if (isNew) boxT.Find("InfoText").GetComponent<Text>().text = "New Pet!";
        else boxT.Find("InfoText").GetComponent<Text>().text = "";
        boxT.Find("PetInfo").GetComponent<Image>().sprite = petAtlas.GetSprite(p);
        boxT.Find("New").gameObject.SetActive(true);
    }

    IEnumerator ShowRemainTimeRoutine(string p, TimeSpan remain, int idx)
    {
        while (remain.TotalSeconds > 0)
        {
            workText.text = remain.ToString(@"hh\:mm\:ss");
            yield return new WaitForSeconds(1);
            remain = remain.Subtract(new TimeSpan(0, 0, 1));
        }
        SetRewardStatus(p, PetEffect.Instance.work.Find(w => w.type == p).earn[Data.Instance.myPet.pets[p].level], idx);
    }

    public void Exit()
    {
        Close(false);
        timeMng.PlayTime();
    }

    override public void Close(bool reset)
    {
        base.Close(reset);
        box.SetActive(false);
        petOn.SetActive(false);
        boxOn.SetActive(false);
        StopAllCoroutines();
        petInfo.gameObject.SetActive(false);
    }

    IEnumerator NoMoneyRoutine(int cost)
    {
        levelUpCostText.text = "Not enough coin";
        yield return new WaitForSeconds(1.5f);
        levelUpCostText.text = cost.ToString();
    }
}