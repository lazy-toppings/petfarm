﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class PopupMng : MonoBehaviour
{
    public GameObject popup;
    public RectTransform content;
    
    protected float deltaX;
    protected float deltaY;

    protected float nextY = 0;

    public virtual void Close(bool reset = true)
    {
        popup.SetActive(false);
        if (reset)
        {
            for (int i = 0; i < content.childCount; ++i)
            {
                content.GetChild(i).gameObject.SetActive(false);
            }

            content.sizeDelta = new Vector2(deltaX, deltaY);
            nextY = 0;
        }
        content.localPosition = new Vector2(0, 0);
    }
}