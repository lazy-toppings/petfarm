﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FenceBuildMng : MonoBehaviour, IPointerClickHandler
{
    public MainMng mainMng;
    
    private Buildings? b;
    private int x = -1;
    private int y;
    private int cost;
    private bool isBuild;

    private int prevX;
    private int prevY;

    private GameObject build;

    public void OnPointerClick(PointerEventData eventData)
    {
        GameObject selected = eventData.pointerCurrentRaycast.gameObject;

        prevX = x; prevY = y;

        x = Int32.Parse(selected.name);
        y = Int32.Parse(selected.transform.parent.name);
        
        if (x == prevX && y == prevY)
        {
            if (isBuild) BuyBuilding();
            else BuyDestroy();
        }
        else
        {
            build.SetActive(false);
            if (isBuild) BuildBuilding(b.GetValueOrDefault(), false, cost);
            else DestroyBuilding(false, cost);
        }
    }

    public void BuildBuilding(Buildings building, bool first, int cost)
    {
        if (first)
        {
            SetGround(true);
            int[] xy = Data.Instance.groundData.GetEmptySpace();

            x = xy[0];
            y = xy[1];
            b = building;
            this.cost = cost;
            isBuild = true;
        }

        if (build != null) build.SetActive(false);
        build = mainMng.BuildTransBuilding(b.ToString(), x, y);
    }

    public void DestroyBuilding(bool first, int cost)
    {
        if (first)
        {
            SetGround(false);
            int[] xy = Data.Instance.groundData.GetEmptyBuilding();
            this.cost = cost;
            x = xy[0];
            y = xy[1];
            b = Buildings.Destory;
            isBuild = false;
        }

        if (build != null) build.SetActive(false);
        build = mainMng.SetBuilding(b.ToString(), x, y);
    }

    private void BuyBuilding()
    {
        GetComponent<AudioSource>().Play();
        Data.Instance.inventory.AddMoney(-cost);
        Data.Instance.groundData.BuildBuilding(x, y, b.GetValueOrDefault());
        mainMng.SetBuilding(b.GetValueOrDefault().ToString(), x, y);
        Data.Instance.logData.MakeGround();
        Close();
    }

    private void BuyDestroy()
    {
        GetComponent<AudioSource>().Play();
        Data.Instance.inventory.AddMoney(-cost);
        Data.Instance.groundData.RemoveBuilding(x, y);
        mainMng.RemoveBuilding(x, y);
        Data.Instance.logData.DestroyGround();
        Close();
    }

    public void Close()
    {
        x = -1;
        b = null;
        if (build != null) build.SetActive(false);
        SetGroundOff();
    }

    private void SetGround(bool isBuild)
    {
        Buildings[,] buildings = Data.Instance.groundData.gBuildings;
        int[,] pets = Data.Instance.groundData.gPets;
        for (int i = 1; i < 5; ++i)
        {
            Transform groundT = transform.GetChild(i);
            for (int j = 0; j < 3; ++j)
            {
                groundT.GetChild(j).gameObject.SetActive(
                    ((buildings[i, j] == Buildings.Empty) == isBuild)
                    && (isBuild || (pets[i, j] == -1)));
            }
        }
    }

    private void SetGroundOff()
    {
        Buildings[,] buildings = Data.Instance.groundData.gBuildings;
        for (int i = 1; i < 5; ++i)
        {
            Transform groundT = transform.GetChild(i);
            for (int j = 0; j < 3; ++j)
            {
                groundT.GetChild(j).gameObject.SetActive(false);
            }
        }
    }
}
