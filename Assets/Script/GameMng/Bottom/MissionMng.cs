﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.UI;
using Random = System.Random;

public class MissionMng : PopupMng
{
    public TimeMng timeMng;

    public GameObject newMark;
    
    public AudioSource openAudio;
    
    private void Awake()
    {
        deltaX = 270.06f;
        deltaY = 390.9f;
    }

    private void Start()
    {
        SetNewMark();
    }

    public void SetNew()
    {
        newMark.SetActive(true);
    }

    public void OpenMission()
    {
        LoadMission();
        timeMng.PauseTime();
    }

    private void LoadMission()
    {
        Vector2 contentPos = content.localPosition;
        Close();
        content.localPosition = contentPos;
        openAudio.Play();
        popup.SetActive(true);

        var missions = Data.Instance.missionData.missions;
        
        foreach (UserMission m in missions)
        {
            int yDelta;
            GameObject g;
            AMission aMission = Mission.Instance.Of(m);
            if (!m.complete)
            {
                g = Instantiate(Resources.Load("Prefabs/UI/Mission/MissionList")) as GameObject;
                g.GetComponentInChildren<Slider>().value = 1f * m.MatchLog() / aMission.conditionCnt;
                g.transform.Find("CompleteText").GetComponent<Text>().text = $"{m.MatchLog()} / {aMission.conditionCnt}";
            }
            else if (!m.getReward)
            {
                g = Instantiate(Resources.Load("Prefabs/UI/Mission/RewardMissionList")) as GameObject;
                g.GetComponentInChildren<Slider>().value = aMission.conditionCnt / aMission.conditionCnt;
                g.transform.Find("RewardBtn").GetComponent<Button>().onClick.AddListener(() =>
                    GetReward(aMission));
                g.transform.Find("CompleteText").GetComponent<Text>().text = $"{aMission.conditionCnt} / {aMission.conditionCnt}";
            }
            else
            {
                g = Instantiate(Resources.Load("Prefabs/UI/Mission/DoneMissionList")) as GameObject;
                g.transform.Find("CompleteText").GetComponent<Text>().text = $"{aMission.conditionCnt} / {aMission.conditionCnt}";
            }
            g.transform.Find("ConditionText").GetComponent<Text>().text = $"{aMission.condition}";
            Transform rewardBtn = g.transform.Find("RewardBtn");
            rewardBtn.Find("GetRewardText").GetComponent<Text>().text = aMission.rewardAmount.ToString();
            if (aMission.rewardType == "exp") rewardBtn.Find("MoneyImg").gameObject.SetActive(false);
            else rewardBtn.Find("ExpText").gameObject.SetActive(false);

            yDelta = -(int)g.GetComponent<RectTransform>().sizeDelta.y - 35;

            g.transform.SetParent(content);
            g.transform.localPosition = new Vector2(0, nextY);
            g.transform.localScale = new Vector2(1, 1);
            nextY += yDelta;
            if (-nextY > content.sizeDelta.y) content.sizeDelta = new Vector2(content.sizeDelta.x, -nextY);

        }
    }
    
    private void GetReward(AMission mission)
    {
        Data.Instance.missionData.SetGetReward(mission);
        switch (mission.rewardType)
        {
            case "exp": Data.Instance.playData.AddExp(mission.rewardAmount); break;
            case "money": Data.Instance.inventory.AddMoney(mission.rewardAmount); break;
        }
        SetNewMark();
        LoadMission();
    }

    private void SetNewMark()
    {
        if (Data.Instance.missionData.IsNew())
        {
            newMark.SetActive(true);
        }
        else
        {
            newMark.SetActive(false);
        }
    }

    public void Exit()
    {
        timeMng.PlayTime();
        Close();
    }

    public override void Close(bool reload = true)
    {
        base.Close();   
        StopAllCoroutines();
    }

    private IEnumerator WaitSeconds(float seconds, Action action)
    {
        yield return new WaitForSeconds(seconds);
        action();
    }
}
