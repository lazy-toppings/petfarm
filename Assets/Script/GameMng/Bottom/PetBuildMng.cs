﻿using UnityEngine;
using UnityEditor;
using UnityEngine.EventSystems;
using System;

public class PetBuildMng : MonoBehaviour, IPointerClickHandler
{
    public MainMng mainMng;

    private int startDate;
    private Pet p;
    private int x = -1;
    private int y;

    private int prevX;
    private int prevY;

    private GameObject build;

    public void OnPointerClick(PointerEventData eventData)
    {
        GameObject selected = eventData.pointerCurrentRaycast.gameObject;

        prevX = x; prevY = y;

        x = Int32.Parse(selected.name);
        y = Int32.Parse(selected.transform.parent.name);
        
        if (x == prevX && y == prevY)
        {
            BuyPet();
        }
        else
        {
            if (build != null) build.SetActive(false);
            BuildPet(p, false);
        }
    }

    public void BuildPet(Pet pet, bool first, int startDate = 0)
    {
        if (first)
        {
            SetGround(pet);
            int[] xy = Data.Instance.groundData.GetEmptyBuilding(pet.Fences);
            this.startDate = startDate;

            x = xy[0];
            y = xy[1];
            p = pet;
        }

        if (build != null) build.SetActive(false);
        build = mainMng.BulidTransPet(p.kinds.ToString(), x, y);
    }

    public void BuyPet()
    {
        GetComponent<AudioSource>().Play();
        Data.Instance.petData.AddPet(p);
        Data.Instance.groundData.BuildPet(x, y, p.id);
        Data.Instance.messageData.ChangeStatus(p.id, "Accepted");
        Data.Instance.logData.AcceptThePet(p.kinds);

        /*if (Data.Instance.playData.time[0] < 1)
        {
            mainMng.BuildPet(p, x, y);
        }
        else
        {*/
        mainMng.BuildReserved(x, y);
        //}
        Close();
    }

    public void Close()
    {
        x = -1;
        p = null;
        if (build != null) build.SetActive(false);
        SetGroundOff();
    }

    private void SetGround(Pet p)
    {
        Buildings[,] buildings = Data.Instance.groundData.gBuildings;
        int[,] pets = Data.Instance.groundData.gPets;
        for (int i = 1; i < 5; ++i)
        {
            Transform groundT = transform.GetChild(i);
            for (int j = 0; j < 3; ++j)
            {
                groundT.GetChild(j).gameObject.SetActive(p.Fences.Contains(buildings[i, j]) && pets[i, j] == -1);
            }
        }
    }

    private void SetGroundOff()
    {
        Buildings[,] buildings = Data.Instance.groundData.gBuildings;
        for (int i = 1; i < 5; ++i)
        {
            Transform groundT = transform.GetChild(i);
            for (int j = 0; j < 3; ++j)
            {
                groundT.GetChild(j).gameObject.SetActive(false);
            }
        }
    }
}