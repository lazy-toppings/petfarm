﻿using UnityEngine;
using UnityEditor;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;

public class Drag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public ItemBoard itemBoard;
    public Transform pets;

    public string itemKind;

    private GameObject give;
    private Vector2 lastPos;

    void Update()
    {
        if (give != null && lastPos.x > 0 && lastPos.x < Screen.width
            && lastPos.y > 0 && lastPos.y < Screen.height)
        {
            give.transform.position = new Vector2(lastPos.x, lastPos.y);
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (give == null)
        {
            give = new GameObject();
            give.AddComponent<Image>().sprite = GetComponent<Image>().sprite;
            give.transform.SetParent(transform);
            give.transform.position = transform.position;
            give.transform.localScale = new Vector3(1, 1, 1);
            give.GetComponent<RectTransform>().sizeDelta = new Vector2(166, 166);
            give.AddComponent<BoxCollider2D>();
            give.GetComponent<BoxCollider2D>().isTrigger = true;
        }
    }

    public void OnDrag(PointerEventData data)
    {
        lastPos = data.position;
    }

    public void OnEndDrag(PointerEventData data)
    {
        if (give != null)
        {
            int inc = -1, dec = -1;

            //Debug.Log($"position.x = {data.position.x}, position.y = {data.position.y}");
            float axy = itemBoard.gradient * data.position.x + itemBoard.yInter;
            if (data.position.y > axy) inc = 0;
            else if (data.position.y > axy - itemBoard.yLen * 2) inc = 1;
            else if (data.position.y > axy - itemBoard.yLen * 4) inc = 2;
            else if (data.position.y > axy - itemBoard.yLen * 6) inc = 3;
            else inc = 4;

            axy = -itemBoard.gradient * data.position.x + itemBoard.yInter;
            if (data.position.y > axy + itemBoard.yLen * 4) dec = 0;
            else if (data.position.y > axy + itemBoard.yLen * 2) dec = 1;
            else if (data.position.y > axy) dec = 2;
            else if (data.position.y > axy - itemBoard.yLen * 2) dec = 3;
            else dec = 4;

            int x = -1, y = -1;
            if (inc < dec) x = 0; else if (inc == dec) x = 1; else x = 2;
            y = (inc + dec + 1) / 2;

            PetStatusMng status = pets.GetChild(y).GetChild(x).GetComponentInChildren<PetStatusMng>(false);
            
            if (status == null)
            {
                give.SetActive(false);
                give = null;
                return;
            }

            bool ret = status.GetItem(itemKind);
            if (ret)
            {
                int remain = Data.Instance.inventory.UseItemAndRemains(itemKind);
                if (remain != 0) transform.parent.GetComponentInChildren<Text>().text = $"{remain}";
                else itemBoard.LoadCurPage();
            }
            give.SetActive(false);
            give = null;
        }
    }
}
