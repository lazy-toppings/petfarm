﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

public class TutorialMng : MonoBehaviour, IPointerClickHandler
{
    public TimeMng timeMng;
    public MailMng mailMng;
    public ShopMng shopMng;
    public MyPetMng myPetMng;

    public GameObject avoidTouch;
    public GameObject tutoImg;
    public Text content;
    public Transform tutos;

    private Coroutine waitEventRoutine;
    private List<ATutorial> tutorials;
    private int tuto = 0;


    public void StartTutorial()
    {
        gameObject.SetActive(true);
        mailMng.NewPet(1);
        tutorials = Tutorial.Instance.tutorial;
        timeMng.PauseTime();
        ShowTuto(0);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (tutorials[tuto].events)
        {
            StopCoroutine(waitEventRoutine);
            waitEventRoutine = StartCoroutine(EventWaitRoutine());
            return;
        }
        tuto++;


        if (tuto < tutorials.Count) ShowTuto(tuto);
        else
        {
            Data.Instance.playData.DoneTuto();
            gameObject.SetActive(false);
        }
    }

    private void ShowTuto(int idx)
    {
        tutoImg.SetActive(true);
        if (tutorials[idx].progress != "")
        {
            int progress = Convert.ToInt32(tutorials[idx].progress);
            if (progress != 0) tutos.GetChild(progress - 1).gameObject.SetActive(false);
            tutos.GetChild(progress).gameObject.SetActive(true);
        }

        if (idx == 7) StartCoroutine(Check(() => Data.Instance.groundData.OccupiedCount() == 1));
        else if (idx == 10) StartCoroutine(Check(() => Data.Instance.groundData.GetEmptyBuilding()[0] == -1));
        else if (idx == 13) StartCoroutine(Check(() => Data.Instance.inventory.item.Count != 0));
        else if (idx == 17) shopMng.Exit();
        else if (idx == 20) myPetMng.Exit();
        else if (idx == 21)
        {
            Data.Instance.playData.time = new int[2] { 1, 0 };
            timeMng.PlayTime();
        }
        if (tutorials[idx].events) waitEventRoutine = StartCoroutine(EventWaitRoutine());

        content.text = tutorials[idx].content;
    }

    public void ClickEvents(int idx)
    {
        StopCoroutine(waitEventRoutine);
        tuto++;
        ShowTuto(tuto);

        switch (idx)
        {
            case 2: case 8: mailMng.OpenMail(); break;
            case 5: mailMng.LoadFenceList(); break;
            case 61: mailMng.BuyFence(new Beach(), null); break;
            case 62: mailMng.BuyFence(new Forest(), null); break;
            case 9: mailMng.AcceptPet(Data.Instance.messageData.messages[0], null); break;
            case 12: shopMng.OpenShop(); break;
            case 14: shopMng.LoadPetList(); break;
            case 17: myPetMng.OpenPet(); break;
            case 18: myPetMng.LoadBox(); break;
        }
    }

    IEnumerator EventWaitRoutine()
    {
        while (true)
        {
            tutoImg.SetActive(true);
            yield return new WaitForSeconds(3);
            tutoImg.SetActive(false);
            yield return new WaitForSeconds(3);
        }
    }

    IEnumerator Check(Func<bool> check)
    {
        avoidTouch.SetActive(false);
        yield return new WaitUntil(check);
        avoidTouch.SetActive(true);
        StopCoroutine(waitEventRoutine);
        tuto++;
        ShowTuto(tuto);
    }
}
