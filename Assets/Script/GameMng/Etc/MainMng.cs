﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMng : MonoBehaviour
{
    public Transform petT;
    public Transform buildingT;
    public Transform groundT;

    public TutorialMng tutorialMng;
    public ShopMng shopMng;

    public AudioSource likeAudio;
    public AudioSource coinAudio;
    public AudioSource touchAudio;

    private void Awake()
    {
        Debug.Log(Data.Instance.id);
        InitBuildings();
        InitPets();

        if (!Data.Instance.playData.tutoDone) tutorialMng.StartTutorial();
        
    }

    public void InitBuildings()
    {
        Buildings[,] buildings = Data.Instance.groundData.gBuildings;
        for (int y = 1; y < 5; ++y)
        {
            Transform t = buildingT.GetChild(y);
            for (int x = 0; x < 3; ++x)
            {
                Transform child = t.GetChild(x);
                foreach (Transform tmp in child.GetComponentsInChildren<Transform>(false))
                    if (tmp != child) tmp.gameObject.SetActive(false);

                Buildings name = buildings[y, x];
                if (name != Buildings.Empty)
                {
                    GameObject build = Instantiate(Resources.Load("Prefabs/Buildings/" + name.ToString())) as GameObject;
                    Build(build, t, x, y);
                }
            }
        }
    }

    public void InitPets()
    {
        int[,] pets = Data.Instance.groundData.gPets;
        for (int y = 0; y < 5; ++y)
        {
            Transform t = petT.GetChild(y);
            for (int x = 0; x < 3; ++x)
            {
                Transform child = t.GetChild(x);

                string prev = "";
                int prevIdx = -1;
                for (int i = 0; i < child.childCount; ++i)
                {
                    if (child.GetChild(i).gameObject.activeSelf)
                    {
                        prev = child.GetChild(i).name;
                        prevIdx = i;
                    }
                }
                int id = pets[y, x];
                
                if (id != -1)
                {
                    Pet p = Data.Instance.petData.GetPet(id);
                    
                    if (p == null)
                    {
                        Debug.LogError($"Try to get pet with unknown id({id})");
                        continue;
                    }

                    Message m = Data.Instance.messageData.GetMessage(p.id);
                    GameObject build;
                    if (m.startDate > Data.Instance.playData.time[0] && Data.Instance.playData.time[0] > 0)
                    {
                        build = Instantiate(Resources.Load("Prefabs/Pets/Reserved")) as GameObject;
                        Build(build, t, x, y);
                    }
                    else
                    {
                        if (prev != $"{p.kinds}(Clone)")
                        {
                            if (prevIdx != -1) child.GetChild(prevIdx).gameObject.SetActive(false);
                            build = InitPet(p);
                            Build(build, t, x, y);
                        }
                    }
                }
                else
                {
                    if (prevIdx != -1 && child.GetChild(prevIdx).GetComponentInChildren<SpriteRenderer>().color.a == 1)
                    {
                        child.GetChild(prevIdx).gameObject.SetActive(false);
                    }
                }
            }
        }
    }

    private GameObject InitPet(Pet p)
    {
        GameObject build = Instantiate(Resources.Load("Prefabs/Pets/" + p.kinds.ToString())) as GameObject;

        GameObject like = Instantiate(Resources.Load("Prefabs/Ani/LikeAni")) as GameObject;
        like.transform.SetParent(build.transform);

        List<GameObject> coins = new List<GameObject>();
        for (int i = 0; i < 4; ++i) coins.Add(Instantiate(Resources.Load("Prefabs/Ani/CoinAni")) as GameObject);
        Transform coinP = build.transform.Find("Coins");
        foreach (GameObject coin in coins) coin.transform.SetParent(coinP);
                            
        build.GetComponent<PetStatusMng>().info = p;
        build.GetComponent<PetStatusMng>().likeAni = like.GetComponent<Animator>();
        build.GetComponent<PetStatusMng>().likeAudio = likeAudio;
        build.GetComponentInChildren<PetTouchMng>().touchAudio = touchAudio;
        if (shopMng.touchMng.ContainsKey(p.kinds)) shopMng.touchMng[p.kinds].Add(build.GetComponentInChildren<PetTouchMng>());
        else shopMng.touchMng.Add(p.kinds, new List<PetTouchMng>() { build.GetComponentInChildren<PetTouchMng>() });

        PetCoinMng petCoinMng = build.GetComponentInChildren<PetCoinMng>();
        foreach (GameObject coin in coins) petCoinMng.coinAnis.Add(coin.GetComponent<Animator>());
        petCoinMng.coinAudio = coinAudio;

        return build;
    }

    public GameObject SetBuilding(string b, int x, int y)
    {
        GameObject build = Instantiate(Resources.Load("Prefabs/Buildings/" + b)) as GameObject;
        Build(build, buildingT.GetChild(y), x, y);
        return build;
    }

    public void RemoveBuilding(int x, int y)
    {
        Transform parent = buildingT.GetChild(y).GetChild(x);
        foreach (Transform t in parent.GetComponentsInChildren<Transform>(false))
            if (t != parent) t.gameObject.SetActive(false);
    }

    public void SetPet(Pet p, int x, int y)
    {
        GameObject build = Instantiate(Resources.Load("Prefabs/Pets/" + p)) as GameObject;
        build.GetComponent<PetStatusMng>().info = p;
        Build(build, petT.GetChild(y), x, y);
    }

    private void Build(GameObject build, Transform t, int x, int y)
    {
        Vector2 pos = build.transform.localPosition;
        build.transform.SetParent(t.GetChild(x));
        build.transform.localPosition = pos;
        SetSortingLayer(build, y, x);
    }

    private void SetSortingLayer(GameObject g, int layer, int order)
    {
        string layerName = $"G{layer * 2 - ((order == 1) ? 0 : 1)}";
        foreach (SpriteRenderer s in g.GetComponentsInChildren<SpriteRenderer>(true))
        {
            s.sortingLayerName = layerName;
        }
    }

    public void BuildReserved(int x, int y)
    {
        GameObject build = Instantiate(Resources.Load("Prefabs/Pets/Reserved")) as GameObject;
        Build(build, petT.GetChild(y), x, y);
    }
    
    public void BuildPet(Pet p, int x, int y)
    {
        GameObject build = InitPet(p);
        Build(build, petT.GetChild(y), x, y);
    }

    public GameObject BuildTransBuilding(string name, int x, int y)
    {
        GameObject build = Instantiate(Resources.Load("Prefabs/Buildings/" + name.ToString())) as GameObject;
        foreach (SpriteRenderer s in build.GetComponentsInChildren<SpriteRenderer>()) s.color = new Vector4(1, 1, 1, 0.5f);
        Build(build, buildingT.GetChild(y), x, y);
        return build;
    }

    public GameObject BulidTransPet(string name, int x, int y)
    {
        GameObject build = Instantiate(Resources.Load("Prefabs/Pets/" + name)) as GameObject;
        foreach (SpriteRenderer s in build.GetComponentsInChildren<SpriteRenderer>()) s.color = new Vector4(1, 1, 1, 0.5f);
        Build(build, petT.GetChild(y), x, y);
        return build;
    }
}
