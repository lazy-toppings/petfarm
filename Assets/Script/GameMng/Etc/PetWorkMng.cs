﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class PetWorkMng : MonoBehaviour
{
    private GameObject newMark;
    
    private void Start()
    {
        newMark = transform.Find("NewMark").gameObject;

        Dictionary<string, MyPetWork> works = Data.Instance.myPet.works;

        CheckNew();
        foreach (KeyValuePair<string, MyPetWork> w in works)
            if (w.Value.isWork) CheckDone(w.Key, w.Value.workTime);
    }

    public void CheckNew()
    {
        if (Data.Instance.myPet.IsNewReward()) newMark.SetActive(true);
        else newMark.SetActive(false);
    }

    public void CheckDone(string p, DateTime workTime, GameObject newMark = null)
    {
        int time = PetEffect.Instance.work.Find(w => w.type == p).time[Data.Instance.myPet.pets[p].level];
        StartCoroutine(CheckDoneRoutine(p, TimeSpan.FromMinutes(time).Subtract(DateTime.Now.Subtract(workTime)), newMark));
    }

    IEnumerator CheckDoneRoutine(string p, TimeSpan remain, GameObject nm)
    {
        yield return new WaitForSeconds((float)remain.TotalSeconds);
        if (nm != null) nm.SetActive(true);
        Data.Instance.myPet.WorkDone(p);
        newMark.SetActive(true);
        Debug.Log("new");
    }
}
