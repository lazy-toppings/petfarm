﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundMng : MonoBehaviour
{
    public List<AudioSource> music;
    public List<AudioSource> effect;

    private List<float> volume = new List<float>();

    private void Start()
    {
        if (effect.Count != 0) foreach (AudioSource e in effect) volume.Add(e.volume);
        MusicControl(SettingData.Instance.musicOn);
        EffectControl(SettingData.Instance.effectOn);
    }

    public void MusicControl(bool on)
    {
        if (!on) foreach (AudioSource m in music) m.volume = 0;
        else foreach (AudioSource m in music) m.volume = 1;
    }

    public void EffectControl(bool on)
    {
        if (!on) foreach (AudioSource e in effect) e.volume = 0;
        else for (int i = 0; i < effect.Count; ++i) effect[i].volume = volume[i];
    }
}
