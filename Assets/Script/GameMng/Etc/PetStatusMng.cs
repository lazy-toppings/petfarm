﻿using System;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Linq;
using UnityEngine.U2D;
using Random = UnityEngine.Random;
using UnityEngine.EventSystems;

public class PetStatusMng : MonoBehaviour
{
    public SpriteAtlas itemAtlas;

    public Pet info;
    public Animator likeAni;
    public AudioSource likeAudio;

    private GameObject want;
    private string item = null;
    private Coroutine waitRoutine;
    private bool isPause;

    private void Awake()
    {
        want = transform.Find("Want").gameObject;
    }

    void Start()
    {
        if (GetComponentInChildren<SpriteRenderer>().color.a == 1) StartCoroutine(NeedRoutine());
    }

    IEnumerator NeedRoutine()
    {
        float left = RandomWaitSecondsFromCount(Data.Instance.petData.pets.Count);
        while (left > 0)
        {
            yield return new WaitForSeconds(1);
            if (!isPause) left--;
        }
        SetItem();
    }

    private void SetItem()
    {
        info.Want();
        item = $"Item{Random.Range(1, 11)}";
        want.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = itemAtlas.GetSprite(item);
        want.SetActive(true);
        waitRoutine = StartCoroutine(WaitRoutine());
    }
    
    IEnumerator WaitRoutine()
    {
        int remain = 20;
        while (remain > 0)
        {
            yield return new WaitForSeconds(1);
            if (!isPause) remain--;
        }
        want.SetActive(false);
        item = null;
        Data.Instance.logData.MissItem();
        StartCoroutine(NeedRoutine());
    }

    public void Pause()
    {
        isPause = true;
    }

    public void Play()
    {
        isPause = false;
    }

    public bool GetItem(string itemKind)
    {
        if (item != itemKind) return false;

        Data.Instance.logData.GiveTheItem(item);
        likeAni.SetBool("isLike", true);
        likeAudio.Play();
        info.GetItem(itemKind);
        want.SetActive(false);
        item = null;

        StopCoroutine(waitRoutine);
        StartCoroutine(NeedRoutine());
        return true;
    }

    private float RandomWaitSecondsFromCount(int count)
    {
        var factor = (float)Math.Pow(count, 0.7);
        return Random.Range(3 * factor, 5 * factor);
    }
}