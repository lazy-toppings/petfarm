﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Text;
using Newtonsoft.Json;

public class Get : MonoBehaviour
{
    private static string baseUrl = $"{ServerUtil.PETFARM_URL}/v1/data";
    public NotSignedInPopupMng notSignedInPopupMng;

    public GameObject loading;

    public void GetData()
    {
        if (!notSignedInPopupMng.IsChainUser())
        {
            notSignedInPopupMng.Open();
        }
        else
        {
            loading.SetActive(true);
            StartCoroutine(DataGetRoutine());
        }
       
    }

    IEnumerator DataGetRoutine()
    {
        string url = $"{baseUrl}/{Data.Instance.id}";
        UnityWebRequest www = UnityWebRequest.Get(url);

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            loading.SetActive(false);
        }
        else
        {
            loading.SetActive(false);
            string data = www.downloadHandler.text;
            Debug.Log(data);
            Data.Instance.Load(data);
        }
    }
}
