﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class PetCoinMng : MonoBehaviour, IPointerEnterHandler
{
    public List<Animator> coinAnis;
    public AudioSource coinAudio;

    private Coroutine[] coinWaitRoutines = new Coroutine[4];

    void Start()
    {
        if (transform.parent.GetComponentInChildren<SpriteRenderer>().color.a != 1) return;
        StartCoroutine(CoinGiveRoutine());
        for (int i = 0; i < 4; ++i) coinAnis[i].SetInteger("cur", i + 1);
    }

    IEnumerator CoinGiveRoutine()
    {
        while (true)
        {
            int time = Random.Range(3, 10);
            yield return new WaitForSeconds(time);
            GiveCoin();
        }
    }

    IEnumerator CoinWaitRoutine(int pos)
    {
        yield return new WaitForSeconds(20);
        coinAnis[pos-1].SetBool("done", true);
        Data.Instance.logData.MissMoney();
        coinWaitRoutines[pos - 1] = null;
    }

    private void GiveCoin()
    {   
        int pos = Random.Range(1, 5);
        if (coinWaitRoutines[pos - 1] != null) return; 
        coinWaitRoutines[pos-1] = StartCoroutine(CoinWaitRoutine(pos));
        coinAnis[pos-1].SetInteger("pos", pos);
    }

    public void GetCoin(int pos)
    {
        StopCoroutine(coinWaitRoutines[pos-1]);
        coinWaitRoutines[pos - 1] = null;
        coinAudio.Play();

        coinAnis[pos-1].SetBool("done", true);
        int amount = Random.Range(5, 10);
        Data.Instance.inventory.AddMoney(amount);
        Data.Instance.logData.FreeMoney();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        GetCoin(eventData.pointerCurrentRaycast.gameObject.transform.parent.GetComponent<Animator>().GetInteger("cur"));
    }
}
