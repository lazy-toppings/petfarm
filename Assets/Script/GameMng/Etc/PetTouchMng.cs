﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class PetTouchMng : MonoBehaviour, IPointerClickHandler
{
    public string pet;
    public AudioSource touchAudio;

    private Animator animator;
    private List<int> acts;

    private Coroutine touchRoutine;

    private void Awake()
    {
        animator = transform.parent.GetComponent<Animator>();
        SetActs();
    }

    public void SetActs()
    {
        acts = Data.Instance.inventory.ActiveActs(pet, "touch");
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (acts == null || acts.Count == 0) return;
        if (touchRoutine != null) StopCoroutine(touchRoutine);
        touchRoutine = StartCoroutine(TouchRoutine());
    }

    IEnumerator TouchRoutine()
    {
        touchAudio.Play();
        int idx = acts[Random.Range(0, acts.Count)];
        animator.SetInteger("touch", idx);
        yield return new WaitForSeconds(0.5f);
        animator.SetInteger("touch", -1);
    }
}
