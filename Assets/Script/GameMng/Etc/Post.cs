using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Post : MonoBehaviour
{
    private static string baseUrl = $"{ServerUtil.PETFARM_URL}/v1/data";
    public GameObject loading;
    public TimeMng timeMng;
    public NotSignedInPopupMng notSignedInPopupMng;
    public MyPetMng myPetMng;

    public void PostData()
    {
        Open();
        Debug.Log("PostData");
        if (!notSignedInPopupMng.IsChainUser())
        {
            Close();
            notSignedInPopupMng.Open();
        }
        else
        {
            loading.SetActive(true);
            StartCoroutine(DataPostRoutine());
        }
    }

    public void PostBasicBox()
    {
        Open();
        if (!notSignedInPopupMng.IsChainUser())
        {
            Close();
            notSignedInPopupMng.Open();
        }
        else
        {
            loading.SetActive(true);
            StartCoroutine(BasicBoxPostRoutine());
        }
    }
    
    public void PostStandardBox()
    {
        Open();
        if (!notSignedInPopupMng.IsChainUser())
        {
            Close();
            notSignedInPopupMng.Open();
        }
        else
        {
            loading.SetActive(true);
            StartCoroutine(StandardBoxPostRoutine());
        }
    }

    public void DeleteData()
    {
        Open();
        if (!notSignedInPopupMng.IsChainUser())
        {
            Close();
            notSignedInPopupMng.Open();
        }
        else
        {
            loading.SetActive(true);
            StartCoroutine(DataDeleteRoutine());
        }
    }

    IEnumerator BasicBoxPostRoutine()
    {
        string url = $"{baseUrl}/{Data.Instance.id}/boxes?type=Basic";
        

        UnityWebRequest www = UnityWebRequest.Post(url, "");
        www.SetRequestHeader("Accept", "application/json"); 

        yield return www.SendWebRequest();
        

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            loading.SetActive(false);
            Close();
        }
        else
        {
            loading.SetActive(false);
            ChainPet newPet = JsonConvert.DeserializeObject<ChainPet>(www.downloadHandler.text);
            Data.Instance.myPet.Add(newPet);
            Debug.Log("Form upload complete!");
            myPetMng.ShowGetPet(newPet.petType);
            Close();
        }
    }

    IEnumerator StandardBoxPostRoutine()
    {
        string url = $"{baseUrl}/{Data.Instance.id}/boxes?type=Standard";


        UnityWebRequest www = UnityWebRequest.Post(url, "");
        www.SetRequestHeader("Accept", "application/json");

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            loading.SetActive(false);
            Close();
        }
        else
        {
            loading.SetActive(false);
            ChainPet newPet = JsonConvert.DeserializeObject<ChainPet>(www.downloadHandler.text);
            Data.Instance.myPet.Add(newPet);
            Debug.Log("Form upload complete!");
            myPetMng.ShowGetPet(newPet.petType);
            Close();
        }
    }

    IEnumerator DataDeleteRoutine()
    {
        string url = baseUrl;

        UnityWebRequest www = UnityWebRequest.Delete(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            loading.SetActive(false);
            Close();
        }
        else
        {
            loading.SetActive(false);
            Debug.Log("Form upload complete!");
            Close();
        }
    }

    IEnumerator DataPostRoutine()
    {
        string url = baseUrl;
        Debug.Log(url);

        var jsonData = Data.Instance.ToJson();
        Debug.Log(jsonData);

        UnityWebRequest www = UnityWebRequest.Post(url, jsonData);
        www.SetRequestHeader("Accept", "application/json");
        www.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(jsonData));
        www.uploadHandler.contentType = "application/json";

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            // TODO post fail popup
            loading.SetActive(false);
            Debug.Log(www.error);
            Close();
        }
        else
        {
            loading.SetActive(false);
            Debug.Log("Form upload complete!");
            Close();
        }
    }

    private void Open()
    {
        timeMng.PauseTime();
    }

    private void Close()
    {
        timeMng.PlayTime();
    }
}
