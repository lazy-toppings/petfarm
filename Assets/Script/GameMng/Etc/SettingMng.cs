﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class SettingMng : MonoBehaviour
{
    public TimeMng timeMng;
    public SoundMng soundMng;

    public Toggle musicOn;
    public Toggle musicOff;

    public Toggle effectOn;
    public Toggle effectOff;

    private GameObject popup;

    private void Start()
    {
        popup = transform.Find("Popup").gameObject;
        
        musicOn.isOn = SettingData.Instance.musicOn;
        musicOff.isOn = !SettingData.Instance.musicOn;

        effectOn.isOn = SettingData.Instance.effectOn;
        effectOff.isOn = !SettingData.Instance.effectOn;
    }

    public void OpenSetting()
    {
        popup.SetActive(true);
        timeMng.PauseTime();
    }

    public void CloseSettting()
    {
        popup.SetActive(false);
        timeMng.PlayTime();
    }

    public void MusicOn(bool on)
    {
        if (on)
        {
            if (SettingData.Instance.musicOn) return;
            SettingData.Instance.musicOn = true;
            SettingData.Instance.Save();
            soundMng.MusicControl(true);
        }
    }

    public void MusicOff(bool on)
    {
        if (on)
        {
            SettingData.Instance.musicOn = false;
            SettingData.Instance.Save();
            soundMng.MusicControl(false);
        }
    }

    public void EffectOn(bool on)
    {
        if (on)
        {
            if (SettingData.Instance.effectOn) return;
            SettingData.Instance.effectOn = true;
            SettingData.Instance.Save();
            soundMng.EffectControl(true);
        }
    }

    public void EffectOff(bool on)
    {
        if (on)
        {
            SettingData.Instance.effectOn = false;
            SettingData.Instance.Save();
            soundMng.EffectControl(false);
        }
    }
}
