﻿using UnityEngine;
using System.Collections;

public class ForTest : MonoBehaviour
{
    public void OpenAll()
    {
        foreach (string p in FarmConfig.Instance.Pets())
        {
            Data.Instance.myPet.Add(new ChainPet("", p));
        }
    }
    public void LevelUp()
    {
        if (Data.Instance.playData.level == 9) return;
        Data.Instance.playData.AddExp(FarmConfig.Instance.LevelConfig.exp[Data.Instance.playData.level + 1] - Data.Instance.playData.exp);
    }

    public void GetMoney()
    {
        Data.Instance.inventory.AddMoney(10000);
    }
}
