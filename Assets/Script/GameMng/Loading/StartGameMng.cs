﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGameMng : MonoBehaviour
{
    public GameObject loading;
    
    public void Play()
    {
        Debug.Log(SystemInfo.deviceUniqueIdentifier);
        Application.RequestAdvertisingIdentifierAsync(
            (string advertisingId, bool trackingEnabled, string error) =>
            { Debug.Log("advertisingId " + advertisingId + " " + trackingEnabled + " " + error); }
        );
        if (Data.Instance.playData.tutoDone)
        {
            if (Data.Instance.id != null)
            {
                AuthTokenMng authTokenMng = FindObjectOfType<AuthTokenMng>();
                AssetListMng assetListMng = FindObjectOfType<AssetListMng>();
                loading.SetActive(true);
                authTokenMng.GetAuthToken(authTokenResponse =>
                {
                    assetListMng.GetAssetList(authTokenResponse.Token, Data.Instance.id, response =>
                    {
                        loading.SetActive(false);
                        SyncChainPet(response);
                        Debug.Log(response); 
                        
                        SceneManager.LoadScene("MainScene");
                    }, error =>
                    {
                        loading.SetActive(false);
                        Debug.Log(error);
                        SceneManager.LoadScene("MainScene");
                    });
                }, error =>
                {
                    loading.SetActive(false);
                    Debug.Log(error);
                    SceneManager.LoadScene("MainScene");
                });
            }
            else
            {
                SceneManager.LoadScene("MainScene");
            }
        }
        else
        {
            SceneManager.LoadScene("Story0Scene");
        }
    }

    public void Tutorial()
    {
        SceneManager.LoadScene("Story0Scene");
    }

    private void SyncChainPet(AssetListResponse response)
    {
        Dictionary<string, int> counter = new Dictionary<string, int>();
        foreach (Asset asset in response.Results)
        {
            if (FarmConfig.Instance.AvailablePetName(asset.Name))
            {
                string petType = FarmConfig.Instance.PetTypeFromName(asset.Name);
                if (!counter.ContainsKey(petType))
                {
                    counter.Add(petType, 0);
                }

                counter[petType]++;
            }
        }

        MyPet myPet = Data.Instance.myPet;
        List<string> removeQ = new List<string>();
        foreach (string key in myPet.pets.Keys)
        {
            if (counter.ContainsKey(key))
            {
                myPet.pets[key].count = counter[key] + myPet.pets[key].pass;
                counter[key] = -1;
            }
            else
            {
                removeQ.Add(key);
            }
        }

        foreach (string key in removeQ)
        {
            myPet.Remove(key);
        }

        foreach (string key in counter.Keys)
        {
            if (counter[key] == -1) continue;

            myPet.Add(key, counter[key]);
        }
    }
}
