﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WalkingPet : MonoBehaviour
{
    Dictionary<string, Vector2> pos = new Dictionary<string, Vector2>();

    private void Awake()
    {
        foreach (Transform p in transform.Find("Pos").GetComponentsInChildren<Transform>(false))
        {
            pos.Add(p.name, p.position);
        }
    }
    private void Start()
    {
        GeneratePet();
    }

    public void GeneratePet()
    {
        StopAllCoroutines();
        string type = Data.Instance.myPet.GetRandom();
        if (type == null) return;

        GameObject pet = Instantiate(Resources.Load($"Prefabs/MyPet/{type}")) as GameObject;
        InitPet(pet);
    }

    private void InitPet(GameObject pet)
    {
        pet.transform.SetParent(transform);
        pet.transform.position = pos["0"];
        MovePet(pet, 0, 0, 0, 0.7f);
    }

    // layer: 현재 층 start: 현재 위치 dest: 목표 위치
    private void MovePet(GameObject pet, int layer, int start, int dest, float speed)
    {
        if (((layer == 3 || layer == 5 || layer == 7) && (start == 0 || start == 2)) || layer == 8)
        {
            EndPet(pet);
            return;
        }
        int nextDest;
        if (layer % 2 == 1) nextDest = dest + Random.Range(0, 2);
        else nextDest = Random.Range(0, 2);

        foreach (SpriteRenderer s in pet.GetComponentsInChildren<SpriteRenderer>(true))
        {
            s.sortingLayerName = $"G{layer}";
        }
        pet.GetComponent<Movement>().MoveStart(pos[$"{layer + 1}{dest}"], speed, () =>
          {
              MovePet(pet, layer + 1, dest, nextDest, 0.25f);
          });
    }

    private void EndPet(GameObject pet)
    {
        pet.gameObject.SetActive(false);
        StartCoroutine(GenerateInterval());
    }

    IEnumerator GenerateInterval()
    {
        yield return new WaitForSeconds(Random.Range(1, 3));
        GeneratePet();
    }
}
