﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.Networking;
using UnityEngine.UI;

public class BuyBoxMng: MonoBehaviour
{
    private static readonly string BUY_BOX_URL = $"{ServerUtil.PETFARM_URL}/v1/boxes";

    //private string TOKEN_ID = "tok_visa";
    //private int AMOUNT = 100;
    //private string DEVICE_ID = "kay-device-id";
    //private string PASSWORD = "1111";

    public GameObject loading;
    public ErrorPopupMng error;
    public TimeMng timeMng;
    public Post post;
    public MyPetMng myPetMng;
    public Purchaser purchaser;
    
    public InputField passwordInput;

    private string type;
    private string token;
    private string stripe;

    public void OpenPassword(string token, string stripe, string type)
    {
        Open();
        this.token = token;
        this.stripe = stripe;
        this.type = type;
        
        if (type != "Basic" && type != "Standard")
        {
            Close();
            error.SetError("Invalid box type");
        }
        
        //TODO pass form
        purchaser.BuyBox(type);
    }

    public void Open()
    {
        gameObject.SetActive(true);
        if (timeMng != null) timeMng.PauseTime();
    }

    public void Close()
    {
        loading.SetActive(false);
        gameObject.SetActive(false);
        if (timeMng != null) timeMng.PlayTime();
    }

    public void OnEnter()
    {
        loading.SetActive(true);
        StartCoroutine(StandardBoxPostRoutine());
    }

    public void Pass()
    {
        loading.SetActive(true);
        passwordInput.text = Constants.MASTER_PASSWORD;
        StartCoroutine(StandardBoxPostRoutine(true));
    }
    
    IEnumerator StandardBoxPostRoutine(bool pass = false)
    {
        string url = $"{BUY_BOX_URL}?type={type}";
        
        string password = passwordInput.text;
        double price = type == "Basic" ? 1 : 2;
        
        BuyBoxRequestBody body = new BuyBoxRequestBody(token, stripe, Data.Instance.id, price, password);
        string jsonBody = JsonConvert.SerializeObject(body);

        using (UnityWebRequest www = UnityWebRequest.Post(url, jsonBody))
        {
            www.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(jsonBody));
            www.uploadHandler.contentType = "application/json";

            yield return www.SendWebRequest();
            
            loading.SetActive(false);
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                loading.SetActive(false);
                Close();
                if (pass)
                {
                    Debug.Log("Fail to upload chain but passed");
                    ChainPet newPet = new ChainPet(Constants.MASTER_ASSET_ID, RandomUtil.getRandomPet(type));
                    Data.Instance.myPet.Add(newPet);
                    myPetMng.ShowGetPet(newPet.petType);
                }
                else
                {
                    error.SetError("Failed to buy box");
                }
            }
            else
            {
                loading.SetActive(false);
                ChainPet newPet = JsonConvert.DeserializeObject<ChainPet>(www.downloadHandler.text);
                Data.Instance.myPet.Add(newPet);
                Debug.Log("Form upload complete!");
                Close();
                myPetMng.ShowGetPet(newPet.petType);
            }
        }
    }
}
