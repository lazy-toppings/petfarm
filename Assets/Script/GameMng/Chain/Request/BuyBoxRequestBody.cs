using System;
using Newtonsoft.Json;

[Serializable]
public class BuyBoxRequestBody
{
    [JsonProperty] private string token;
    [JsonProperty] private string stripeToken;
    [JsonProperty] private string account;
    [JsonProperty] private double price;
    [JsonProperty] private string password;

    public BuyBoxRequestBody(string token, string stripeToken, string account, double price, string password)
    {
        this.token = token;
        this.stripeToken = stripeToken;
        this.account = account;
        this.price = price;
        this.password = password;
    }
}