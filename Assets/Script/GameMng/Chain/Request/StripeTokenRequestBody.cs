﻿using System;

[Serializable]
public class StripeTokenRequestBody
{
    private string cardNumber;
    private int expMonth;
    private int expYear;
    private int cvc;

    public StripeTokenRequestBody(string cardNumber, int expMonth, int expYear, int cvc)
    {
        this.cardNumber = cardNumber;
        this.expMonth = expMonth;
        this.expYear = expYear;
        this.cvc = cvc;
    }

    public string CardNumber => cardNumber;
    public int ExpMonth => expMonth;
    public int ExpYear => expYear;
    public int Cvc => cvc;
    
    public string ToQueryString()
    {
        return string.Format("card[number]={0}&card[expmonth]={1}&card[expyear]={2}&card[cvc]={3}", cardNumber, expMonth, expYear, cvc);
    }
}
