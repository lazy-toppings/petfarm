using System;
using Newtonsoft.Json;

[Serializable]
public class UserRequestBody
{
    [JsonProperty] private string account;

    public UserRequestBody(string account)
    {
        this.account = account;
    }
}