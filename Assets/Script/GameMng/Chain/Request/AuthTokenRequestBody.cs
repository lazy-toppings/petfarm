using System;
using Newtonsoft.Json;

[Serializable]
public class AuthTokenRequestBody
{
    [JsonProperty] private string deviceId;
    [JsonProperty] private string creator;

    public AuthTokenRequestBody(string deviceId, string creator)
    {
        this.deviceId = deviceId;
        this.creator = creator;
    }
}