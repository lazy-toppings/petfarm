using System;
using UnityEngine;

public class NotSignedInPopupMng: MonoBehaviour
{
    public StripeTokenMng stripeTokenMng;
    public TimeMng timeMng;

    public void Open()
    {
        gameObject.SetActive(true);
        if (timeMng != null) timeMng.PauseTime();
    }
    
    public bool IsChainUser()
    {
        return Data.Instance.id != null;
    }

    public void SignUp()
    {
        Close();
        stripeTokenMng.Open("signup");
    }

    public void Close()
    {
        gameObject.SetActive(false);
        if (timeMng != null) timeMng.PlayTime();
    }
}