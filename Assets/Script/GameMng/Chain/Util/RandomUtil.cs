using UnityEngine;

public class RandomUtil
{
    public static string[] availablePets = {"Pet19", "Pet18", "Pet20", "Pet13", "Pet12", "Pet11", "Pet10", "Pet9", "Pet2", "Pet1"};
    public static int[] seeds = {1, 2, 3, 4, 5};

    public static string getRandomPet(string type)
    {
        if (type == "Basic")
        {
            int value = Random.Range(0, 30);
            for (int i = 0; i < 10; i++)
            {
                if (value <= 0) return availablePets[i];
                value -= seeds[i/2];
            }

            return availablePets[9];
        }
        else
        {
            int value = Random.Range(0, 12);
            for (int i = 0; i < 6; i++)
            {
                if (value <= 0) return availablePets[i];
                value -= seeds[i/2];
            }

            return availablePets[5];
        }
    }
}
    