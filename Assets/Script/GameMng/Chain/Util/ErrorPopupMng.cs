using System;
using UnityEngine;
using UnityEngine.UI;

public class ErrorPopupMng: MonoBehaviour
{
    public TimeMng timeMng;

    public void SetError(string msg)
    {
        transform.Find("ErrorText").GetComponent<Text>().text = $"ERROR: { msg }";
        Open();
    }

    public void Open()
    {
        gameObject.SetActive(true);
        if (timeMng != null) timeMng.PauseTime();
    }

    public void Close()
    {
        gameObject.SetActive(false);
        if (timeMng != null) timeMng.PlayTime();
    }
}