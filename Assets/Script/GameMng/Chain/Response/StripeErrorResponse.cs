﻿using System;
using Newtonsoft.Json;

[Serializable]
public class StripeErrorResponse
{
    [JsonProperty] private ErrorResponse error;

    public StripeErrorResponse(ErrorResponse error)
    {
        this.error = error;
    }

    public ErrorResponse Error => error;

    public class ErrorResponse
    {
        [JsonProperty] private string code;
        [JsonProperty(propertyName: "doc_url")] private string docUrl;
        [JsonProperty] private string message;
        [JsonProperty] private string param;
        [JsonProperty] private string type;

        public ErrorResponse(string code, string docUrl, string message, string param, string type)
        {
            this.code = code;
            this.docUrl = docUrl;
            this.message = message;
            this.param = param;
            this.type = type;
        }

        public string Code => code;
        public string DocUrl => docUrl;
        public string Message => message;
        public string Param => param;
        public string Type => type;
    }
}
