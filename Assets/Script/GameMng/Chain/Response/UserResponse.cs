﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

[Serializable]
public class UserResponse
{
    [JsonProperty] private string id;
    [JsonProperty] private string account;

    public UserResponse(string id, string account)
    {
        this.id = id;
        this.account = account;
    }

    public string Id => id;
    public string Account => account;
}

