﻿using System;
using Newtonsoft.Json;

[Serializable]
public class StripeTokenResponse
{
    [JsonProperty] private string id;

    public StripeTokenResponse(string id)
    {
        this.id = id;
    }

    public string Id => id;
}
