﻿using System;
using Newtonsoft.Json;
using UnityEngine;

[Serializable]
public class SignUpErrorResponse
{
    [JsonProperty] private string msg;
    [JsonProperty] private ErrorDetail detail;

    public SignUpErrorResponse(string msg, ErrorDetail detail)
    {
        this.msg = msg;
        this.detail = detail;
    }

    public string Msg => msg;

    public ErrorDetail Detail => detail;

    public class ErrorDetail
    {
        [JsonProperty] private string address;
        [JsonProperty] private string deviceId;

        public ErrorDetail(string address, string deviceId)
        {
            this.address = address;
            this.deviceId = deviceId;
        }

        public string Address => address;

        public string DeviceId => deviceId;
    }
}
