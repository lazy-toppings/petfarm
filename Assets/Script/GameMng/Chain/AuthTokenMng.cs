using System.Collections;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

public class AuthTokenMng: MonoBehaviour
{
    private static readonly string GET_AUTH_TOKEN_URL = $"{ServerUtil.GAMERECIPE_URL}/auth/token";
    private static readonly string CREATOR = "lazytoppings";
    private string authToken;

    public string AuthToken => authToken;

    public void GetAuthToken(System.Action<AuthTokenResponse> callback, System.Action<string> errorCallback)
    {
        Application.RequestAdvertisingIdentifierAsync(
            (string advertisingId, bool trackingEnabled, string error) =>
            {
                Debug.Log("advertisingId " + advertisingId + " " + trackingEnabled + " " + error);
                // string deviceId = SystemInfo.deviceUniqueIdentifier;
                string deviceId = advertisingId;
                AuthTokenRequestBody requestBody = new AuthTokenRequestBody(deviceId, CREATOR);
                string jsonData = JsonConvert.SerializeObject(requestBody);
                StartCoroutine(RequestAuthToken(GET_AUTH_TOKEN_URL, jsonData, callback, errorCallback));
            }
        );
    }

    public void GetAuthTokenOnly(System.Action<string> callback, System.Action<string> errorCallback)
    {
        if (authToken != null)
        {
            callback(authToken);
        }
        else
        {
            GetAuthToken(response => { callback(response.Token); }, errorCallback);
        }
    }

    private IEnumerator RequestAuthToken(string uri, string jsonData, System.Action<AuthTokenResponse> callback, 
        System.Action<string> errorCallback)
    {
        using (UnityWebRequest www = UnityWebRequest.Post(uri, jsonData))
        {
            www.uploadHandler.contentType = "application/json";
            www.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(jsonData));
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                string jsonResult = Encoding.UTF8.GetString(www.downloadHandler.data);
                errorCallback(jsonResult);
            }
            else
            {
                string jsonResult = Encoding.UTF8.GetString(www.downloadHandler.data);
                AuthTokenResponse response = JsonConvert.DeserializeObject<AuthTokenResponse>(jsonResult);
                authToken = response.Token;
                callback(response);
            }
        }
    }
}