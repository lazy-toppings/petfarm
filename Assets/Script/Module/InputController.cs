﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;

public class InputController : MonoBehaviour
{
    private bool isClick = false;

    private void Update()
    {
        if (isClick)
        {
            Exit();
            return;
        }

        if (Input.GetKey(KeyCode.Escape))
        {
            isClick = true;
            
            StartCoroutine(WaitSeconds(0.3f, () =>
            {
                isClick = false;
            }));
        }
    }

    public void Exit()
    {
        Application.Quit();
    }

    IEnumerator WaitSeconds(float sec, Action action)
    {
        yield return new WaitForSeconds(sec);
        action();
    }
}