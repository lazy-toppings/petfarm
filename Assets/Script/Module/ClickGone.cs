﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class ClickGone : MonoBehaviour
{
    public TimeMng timeMng;

    private bool canTouch = false;

    void OnEnable()
    {
        canTouch = false;
        timeMng.PauseTime();
        StartCoroutine(WaitSeconds(1f));
    }

    public void Gone()
    {
        if (canTouch)
        {
            timeMng.PlayTime();
            gameObject.SetActive(false);
        }
    }

    IEnumerator WaitSeconds(float sec)
    {
        yield return new WaitForSeconds(sec);
        canTouch = true;
    }
}