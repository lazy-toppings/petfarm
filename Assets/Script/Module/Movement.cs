﻿using UnityEngine;
using System.Collections;
using System;

public class Movement : MonoBehaviour
{
    private bool isMoving = false;
    private float timer;
    private float speed;
    private Vector2 now;
    private Vector2 move = new Vector2(0, 0);
    private Action afterDone;

    void Update()
    {
        if (isMoving)
        {
            Move();
        }
    }

    public void MoveStart(Vector2 move, float speed, Action afterDone)
    {
        now = transform.position;
        this.speed = speed;
        this.move = move;
        this.afterDone = afterDone;

        timer = 0;
        isMoving = true;
    }

    private void Move()
    {
        if (timer > 1f)
        {
            isMoving = false;
            afterDone();
            return;
        }

        timer += Time.deltaTime * speed;
        transform.position = Vector3.Lerp(now, new Vector2(move.x, move.y), timer);
    }
}
